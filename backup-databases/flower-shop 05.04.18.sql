-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1
-- Время создания: Апр 05 2018 г., 19:58
-- Версия сервера: 10.1.26-MariaDB
-- Версия PHP: 7.1.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `flower-shop`
--

-- --------------------------------------------------------

--
-- Структура таблицы `all_images`
--

CREATE TABLE `all_images` (
  `id` int(11) NOT NULL,
  `id_item` int(11) NOT NULL,
  `url` varchar(1024) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `all_images`
--

INSERT INTO `all_images` (`id`, `id_item`, `url`) VALUES
(1, 8, 'C:/xampp/htdocs/flower-online-store-niva/current-files/uploaded-images/1522949559-2.jpg'),
(2, 8, 'C:/xampp/htdocs/flower-online-store-niva/current-files/uploaded-images/1522949559-3.jpg'),
(3, 8, 'C:/xampp/htdocs/flower-online-store-niva/current-files/uploaded-images/1522949559-4.jpg'),
(4, 8, 'C:/xampp/htdocs/flower-online-store-niva/current-files/uploaded-images/1522949559-5.jpg'),
(5, 9, 'C:/xampp/htdocs/flower-online-store-niva/current-files/uploaded-images/1522950231-6.jpg'),
(6, 9, 'C:/xampp/htdocs/flower-online-store-niva/current-files/uploaded-images/1522950231-7.jpg'),
(7, 9, 'C:/xampp/htdocs/flower-online-store-niva/current-files/uploaded-images/1522950231-8.jpg'),
(8, 9, 'C:/xampp/htdocs/flower-online-store-niva/current-files/uploaded-images/1522950231-9.jpg'),
(9, 10, 'C:/xampp/htdocs/flower-online-store-niva/current-files/uploaded-images/1522950425-6.jpg'),
(10, 10, 'C:/xampp/htdocs/flower-online-store-niva/current-files/uploaded-images/1522950425-7.jpg'),
(11, 10, 'C:/xampp/htdocs/flower-online-store-niva/current-files/uploaded-images/1522950425-8.jpg'),
(12, 10, 'C:/xampp/htdocs/flower-online-store-niva/current-files/uploaded-images/1522950425-9.jpg'),
(13, 11, 'C:/xampp/htdocs/flower-online-store-niva/current-files/uploaded-images/1522950594-2.jpg'),
(14, 11, 'C:/xampp/htdocs/flower-online-store-niva/current-files/uploaded-images/1522950594-3.jpg'),
(15, 11, 'C:/xampp/htdocs/flower-online-store-niva/current-files/uploaded-images/1522950594-4.jpg'),
(16, 11, 'C:/xampp/htdocs/flower-online-store-niva/current-files/uploaded-images/1522950594-5.jpg'),
(17, 11, 'C:/xampp/htdocs/flower-online-store-niva/current-files/uploaded-images/1522950594-6.jpg'),
(18, 11, 'C:/xampp/htdocs/flower-online-store-niva/current-files/uploaded-images/1522950594-7.jpg'),
(19, 11, 'C:/xampp/htdocs/flower-online-store-niva/current-files/uploaded-images/1522950594-8.jpg'),
(20, 12, 'C:/xampp/htdocs/flower-online-store-niva/current-files/uploaded-images/1522950839-1.jpg'),
(21, 12, 'C:/xampp/htdocs/flower-online-store-niva/current-files/uploaded-images/1522950839-2.jpg'),
(22, 12, 'C:/xampp/htdocs/flower-online-store-niva/current-files/uploaded-images/1522950839-3.jpg'),
(23, 12, 'C:/xampp/htdocs/flower-online-store-niva/current-files/uploaded-images/1522950839-4.jpg'),
(24, 12, 'C:/xampp/htdocs/flower-online-store-niva/current-files/uploaded-images/1522950839-5.jpg'),
(25, 12, 'C:/xampp/htdocs/flower-online-store-niva/current-files/uploaded-images/1522950839-6.jpg'),
(26, 12, 'C:/xampp/htdocs/flower-online-store-niva/current-files/uploaded-images/1522950839-7.jpg'),
(27, 12, 'C:/xampp/htdocs/flower-online-store-niva/current-files/uploaded-images/1522950839-8.jpg'),
(28, 12, 'C:/xampp/htdocs/flower-online-store-niva/current-files/uploaded-images/1522950839-9.jpg'),
(29, 13, 'C:/xampp/htdocs/flower-online-store-niva/current-files/uploaded-images/1522950917-1.jpg'),
(30, 13, 'C:/xampp/htdocs/flower-online-store-niva/current-files/uploaded-images/1522950917-2.jpg'),
(31, 13, 'C:/xampp/htdocs/flower-online-store-niva/current-files/uploaded-images/1522950917-3.jpg'),
(32, 13, 'C:/xampp/htdocs/flower-online-store-niva/current-files/uploaded-images/1522950917-4.jpg'),
(33, 13, 'C:/xampp/htdocs/flower-online-store-niva/current-files/uploaded-images/1522950917-5.jpg'),
(34, 13, 'C:/xampp/htdocs/flower-online-store-niva/current-files/uploaded-images/1522950917-6.jpg'),
(35, 13, 'C:/xampp/htdocs/flower-online-store-niva/current-files/uploaded-images/1522950917-7.jpg'),
(36, 13, 'C:/xampp/htdocs/flower-online-store-niva/current-files/uploaded-images/1522950917-8.jpg'),
(37, 13, 'C:/xampp/htdocs/flower-online-store-niva/current-files/uploaded-images/1522950917-9.jpg');

-- --------------------------------------------------------

--
-- Структура таблицы `all_items`
--

CREATE TABLE `all_items` (
  `id` int(11) NOT NULL,
  `title` varchar(64) NOT NULL,
  `cost` int(11) NOT NULL,
  `mini_image_url` varchar(512) NOT NULL,
  `type_of_item` enum('normal','set') NOT NULL DEFAULT 'normal',
  `mini_description` varchar(128) DEFAULT NULL,
  `description` mediumtext,
  `is_hot` tinyint(1) NOT NULL DEFAULT '0',
  `is_sale` tinyint(1) NOT NULL DEFAULT '0',
  `last_cost` int(11) DEFAULT NULL,
  `date_add` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `all_items`
--

INSERT INTO `all_items` (`id`, `title`, `cost`, `mini_image_url`, `type_of_item`, `mini_description`, `description`, `is_hot`, `is_sale`, `last_cost`, `date_add`) VALUES
(1, 'Тестовый товар из базы данных 1-p', 9000, 'default-product.png', 'normal', NULL, 'Тестовый товар из базы данных', 1, 1, 90000, 1),
(2, 'Тестовый товар из базы данных 2-p', 9000, 'default-product.png', 'normal', NULL, 'Тестовый товар из базы данных', 1, 0, NULL, 2),
(3, 'Тестовый товар из базы данных 3-p', 9000, 'default-product.png', 'normal', NULL, 'Тестовый товар из базы данных', 1, 0, NULL, 3),
(4, 'Тестовый товар из базы данных 4-p', 9000, 'default-product.png', 'normal', NULL, 'Тестовый товар из базы данных', 1, 0, NULL, 4),
(5, 'Тестовый товар из базы данных 1-s', 6000, 'default-product.png', 'normal', NULL, 'Тестовый товар из базы данных', 0, 1, 9000, 5),
(6, 'Тестовый товар из базы данных 2-s', 6000, 'default-product.png', 'normal', NULL, 'Тестовый товар из базы данных', 0, 1, 9000, 5),
(7, 'Тестовый набор из базы данных 1', 99836, 'default-product.png', 'set', 'Используемые цветы: лилии, ромашки, одуванчики, гибискусы', 'Используемые цветы: лилии, ромашки, одуванчики, гибискусы Полное описаниеПолное описаниеПолное описаниеПолное описаниеПолное описаниеПолное описаниеПолное описаниеПолное описаниеПолное описаниеПолное описаниеПолное описаниеПолное описаниеПолное описаниеПолное описаниеПолное описаниеПолное описаниеПолное описаниеПолное описание', 1, 1, 100000, 90),
(8, 'Добавленный через панель', 99999999, '1522949559-1.jpg', 'normal', 'Краткое. Добавленный через панель Добавленный через панель Добавленный через панель Добавленный через панель', 'Полное. Добавленный через панель Добавленный через панель Добавленный через панель Добавленный через панель Добавленный через панель Добавленный через панель Добавленный через панель Добавленный через панель Добавленный через панель ', 0, 0, NULL, 100),
(13, 'Набор', 34556677, '1522950917-8.jpg', 'set', 'Набор. Краткое', 'Набор. Полное', 0, 0, NULL, 1522950917);

-- --------------------------------------------------------

--
-- Структура таблицы `all_users`
--

CREATE TABLE `all_users` (
  `id` int(11) NOT NULL,
  `login` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `access` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `all_users`
--

INSERT INTO `all_users` (`id`, `login`, `password`, `access`) VALUES
(1, 'Admin', '2c7a5a6bfa4b5baee3b981b7803c3747', 10);

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `all_images`
--
ALTER TABLE `all_images`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `all_items`
--
ALTER TABLE `all_items`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `all_users`
--
ALTER TABLE `all_users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `all_images`
--
ALTER TABLE `all_images`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;
--
-- AUTO_INCREMENT для таблицы `all_items`
--
ALTER TABLE `all_items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT для таблицы `all_users`
--
ALTER TABLE `all_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
