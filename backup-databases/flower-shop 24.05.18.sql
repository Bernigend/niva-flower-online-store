-- phpMyAdmin SQL Dump
-- version 4.7.3
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3306
-- Время создания: Май 24 2018 г., 18:43
-- Версия сервера: 5.7.19
-- Версия PHP: 7.1.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `flower-shop`
--

-- --------------------------------------------------------

--
-- Структура таблицы `all_images`
--

CREATE TABLE `all_images` (
  `id` int(11) NOT NULL,
  `id_item` int(11) NOT NULL,
  `url` varchar(1024) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `all_images`
--

INSERT INTO `all_images` (`id`, `id_item`, `url`) VALUES
(51, 19, '/uploaded-images/1525707645-1522949559-5.jpg'),
(52, 20, '/uploaded-images/1527002763-product-1.png'),
(53, 20, '/uploaded-images/1527002763-product-2.png'),
(54, 20, '/uploaded-images/1527002763-product-3.png'),
(55, 21, '/uploaded-images/1527002831-product-1.png'),
(56, 21, '/uploaded-images/1527002831-product-2.png'),
(57, 21, '/uploaded-images/1527002831-product-3.png'),
(58, 22, '/uploaded-images/1527002941-product-1.png'),
(59, 22, '/uploaded-images/1527002941-product-2.png'),
(60, 22, '/uploaded-images/1527002941-product-3.png'),
(61, 23, '/uploaded-images/1527003022-product-1.png'),
(62, 23, '/uploaded-images/1527003022-product-2.png'),
(63, 23, '/uploaded-images/1527003022-product-3.png'),
(64, 24, '/uploaded-images/1527003136-product-1.png'),
(65, 24, '/uploaded-images/1527003136-product-2.png'),
(66, 24, '/uploaded-images/1527003136-product-3.png');

-- --------------------------------------------------------

--
-- Структура таблицы `all_items`
--

CREATE TABLE `all_items` (
  `id` int(11) NOT NULL,
  `title` varchar(64) NOT NULL,
  `cost` int(11) NOT NULL,
  `mini_image_url` varchar(512) NOT NULL,
  `type_of_item` enum('normal','set') NOT NULL DEFAULT 'normal',
  `mini_description` varchar(256) DEFAULT NULL,
  `description` mediumtext,
  `is_hot` tinyint(1) NOT NULL DEFAULT '0',
  `sale_time` int(11) NOT NULL DEFAULT '0',
  `sale_cost` int(11) DEFAULT '0',
  `date_add` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `all_items`
--

INSERT INTO `all_items` (`id`, `title`, `cost`, `mini_image_url`, `type_of_item`, `mini_description`, `description`, `is_hot`, `sale_time`, `sale_cost`, `date_add`) VALUES
(20, 'Растения жёлтые', 5500, '1527002763-product-1.png', 'normal', 'Краткое описание жёлтого растения', 'Полное описание жёлтого растения', 0, 1608411600, 2900, 1527002763),
(21, 'Растения лазурные', 7000, '1527002831-product-2.png', 'normal', 'Краткое описание лазурного растения', 'Полное описание лазурного растения', 0, 1608411600, 4500, 1527002831),
(22, 'Растения бордовые', 12350, '1527002941-product-3.png', 'normal', 'Краткое описание бордового растения', 'Полное описание бордового растения', 0, 1608411600, 9700, 1527002941),
(23, 'Лазурный набор', 23690, '1527003022-product-2.png', 'set', 'Краткое описание лазурного набора, который прекрасно подойдёт в качестве подарка вашей любимой.', 'Полное описание лазурного набора, который прекрасно подойдёт в качестве подарка вашей любимой.', 0, 0, 0, 1527003022),
(24, 'Бордовый набор', 56800, '1527003136-product-3.png', 'set', 'Краткое описание бордового набора, который прекрасно подойдёт в качестве подарка вашей любимой.', 'Полное описание бордового набора, который прекрасно подойдёт в качестве подарка вашей любимой.', 0, 0, 0, 1527003136),
(25, 'Растение жёлтое', 5500, '1527002763-product-1.png', 'normal', 'Краткое описание жёлтого растения', 'Полное описание жёлтого растения', 1, 0, 0, 1527002946),
(26, 'Растение лазурное', 7000, '1527002831-product-2.png', 'normal', 'Краткое описание лазурного растения', 'Полное описание лазурного растения', 1, 0, 0, 1527002949),
(27, 'Растение бордовое', 12350, '1527002941-product-3.png', 'normal', 'Краткое описание бордового растения', 'Полное описание бордового растения', 1, 0, 0, 1527002950);

-- --------------------------------------------------------

--
-- Структура таблицы `all_settings`
--

CREATE TABLE `all_settings` (
  `id` int(11) NOT NULL,
  `name` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `value` longtext COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `all_settings`
--

INSERT INTO `all_settings` (`id`, `name`, `value`) VALUES
(1, 'slider_status', 'on'),
(2, 'page_about_us_content', '&lt;p&gt;&lt;strong&gt;Страница &quot;О нас&quot;&lt;/strong&gt;&lt;/p&gt;'),
(3, 'page_delivery_content', '&lt;p&gt;&lt;strong&gt;Страница &quot;Доставка&quot;&lt;/strong&gt;&lt;/p&gt;'),
(4, 'page_contacts_content', '&lt;p&gt;&lt;strong&gt;Страница &quot;Контакты&quot;&lt;/strong&gt;&lt;/p&gt;\r\n&lt;script type=&quot;text/javascript&quot; src=&quot;https://api-maps.yandex.ru/services/constructor/1.0/js/?um=constructor%3A0f83714e0202c4cac3dfbb9906ad0b5caa0145dbfffc58464aa6d6d96be21120&amp;amp;width=100%25&amp;amp;height=450&amp;amp;lang=ru_RU&amp;amp;scroll=true&quot;&gt;&lt;/script&gt;');

-- --------------------------------------------------------

--
-- Структура таблицы `all_users`
--

CREATE TABLE `all_users` (
  `id` int(11) NOT NULL,
  `login` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `access` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `all_users`
--

INSERT INTO `all_users` (`id`, `login`, `password`, `access`) VALUES
(1, 'Admin', '2c7a5a6bfa4b5baee3b981b7803c3747', 10);

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `all_images`
--
ALTER TABLE `all_images`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `all_items`
--
ALTER TABLE `all_items`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `all_settings`
--
ALTER TABLE `all_settings`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `all_users`
--
ALTER TABLE `all_users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `all_images`
--
ALTER TABLE `all_images`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=67;
--
-- AUTO_INCREMENT для таблицы `all_items`
--
ALTER TABLE `all_items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;
--
-- AUTO_INCREMENT для таблицы `all_settings`
--
ALTER TABLE `all_settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT для таблицы `all_users`
--
ALTER TABLE `all_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
