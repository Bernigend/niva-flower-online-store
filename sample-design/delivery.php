<!--
 - Created by Bernigend, https://bernigend.ru || https://vk.com/bernigend
 - Created on 03/2018 for the project of the store
-->

<!DOCTYPE html>
<html lang="ru" dir="ltr">

<head>

	<meta charset="UTF-8">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="Описание страницы короче 150 символов">

	<link href="https://fonts.googleapis.com/css?family=Open+Sans:300" rel="stylesheet">
	<link rel="stylesheet" href="style/standart.css?98776">
	<link rel="icon" type="image/png" href="images/favicon.png">

	<title>Доставка - Интернет-магазин цветов в Сергиевом Посаде</title>

</head>

<body>

<!-- Заголовок страницы + навигация верхнего уровня -->

	<header>

		<div class="logo"></div>

		<nav id="js-navigation">
			<ul>
				<li><a href="index.php">Главная</a></li>
				<li><a href="about-us.php">О нас</a></li>
				<li><a href="catalog.php">Каталог</a></li>
				<li><a class="active" href="delivery.php">Доставка</a></li>
				<li><a href="contacts.php">Контакты</a></li>
			</ul>
		</nav>
		
	</header>

<!-- Главный слайдер -->
		
	<div class="slider">
		<div class="all-slides">
			<div class="slide" style="background-image: url(images/default-slide-1.png)"></div><div class="slide" style="background-image: url(images/default-slide-1.png)"></div><div class="slide" style="background-image: url(images/default-slide-1.png)"></div><div class="slide" style="background-image: url(images/default-slide-1.png)"></div>
		</div>
		<div class="next" onclick="sliderNext ();"></div>
		<div class="last" onclick="sliderLast ();"></div>
	</div>

<!-- Основной контент страницы -->

	<main class="wrapper with-backg with-mg-bt with-all-pd">

		<h3 class="title">О доставке</h3>
		<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. In a nisi tincidunt, feugiat nulla at, dictum orci. Maecenas venenatis dolor urna, sed dictum nisl pulvinar id. Vestibulum at purus diam. Etiam consectetur maximus lacus sed tincidunt. Vestibulum malesuada sapien est, egestas congue tortor egestas ac. Vivamus at augue sagittis, sollicitudin mauris nec, vulputate est. In sed nulla ut libero ornare sollicitudin.</p>
		<p>Duis id luctus tortor, facilisis varius risus. In malesuada fringilla velit, non suscipit massa accumsan et. Aliquam bibendum, ipsum sit amet fringilla gravida, velit sem iaculis risus, nec luctus nisi augue eu mi. Etiam id tortor vel turpis porttitor convallis. Nulla porta est ex, ac interdum purus pulvinar ut. Etiam id sem porta, blandit eros sed, pellentesque tortor. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus commodo risus nec ante consequat, vitae condimentum neque imperdiet. Quisque id pulvinar ante, imperdiet volutpat purus. Praesent viverra vulputate lacus a malesuada. Proin rhoncus sapien ut odio semper, vitae aliquet dui eleifend. Vestibulum at dignissim orci, nec vulputate neque. Donec semper quam sit amet eros blandit, quis sollicitudin nisi facilisis. In hendrerit tortor placerat nisl varius, eu scelerisque velit rutrum.</p>

		<hr>

		<h3 class="title">Правила доставки</h3>
		<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. In a nisi tincidunt, feugiat nulla at, dictum orci. Maecenas venenatis dolor urna, sed dictum nisl pulvinar id. Vestibulum at purus diam. Etiam consectetur maximus lacus sed tincidunt. Vestibulum malesuada sapien est, egestas congue tortor egestas ac. Vivamus at augue sagittis, sollicitudin mauris nec, vulputate est. In sed nulla ut libero ornare sollicitudin.</p>
		<p>Duis id luctus tortor, facilisis varius risus. In malesuada fringilla velit, non suscipit massa accumsan et. Aliquam bibendum, ipsum sit amet fringilla gravida, velit sem iaculis risus, nec luctus nisi augue eu mi. Etiam id tortor vel turpis porttitor convallis. Nulla porta est ex, ac interdum purus pulvinar ut. Etiam id sem porta, blandit eros sed, pellentesque tortor. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus commodo risus nec ante consequat, vitae condimentum neque imperdiet. Quisque id pulvinar ante, imperdiet volutpat purus. Praesent viverra vulputate lacus a malesuada. Proin rhoncus sapien ut odio semper, vitae aliquet dui eleifend. Vestibulum at dignissim orci, nec vulputate neque. Donec semper quam sit amet eros blandit, quis sollicitudin nisi facilisis. In hendrerit tortor placerat nisl varius, eu scelerisque velit rutrum.</p>

		<hr>

		<h3 class="title">Способы доставки</h3>
		<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. In a nisi tincidunt, feugiat nulla at, dictum orci. Maecenas venenatis dolor urna, sed dictum nisl pulvinar id. Vestibulum at purus diam. Etiam consectetur maximus lacus sed tincidunt. Vestibulum malesuada sapien est, egestas congue tortor egestas ac. Vivamus at augue sagittis, sollicitudin mauris nec, vulputate est. In sed nulla ut libero ornare sollicitudin.</p>
		<p>Duis id luctus tortor, facilisis varius risus. In malesuada fringilla velit, non suscipit massa accumsan et. Aliquam bibendum, ipsum sit amet fringilla gravida, velit sem iaculis risus, nec luctus nisi augue eu mi. Etiam id tortor vel turpis porttitor convallis. Nulla porta est ex, ac interdum purus pulvinar ut. Etiam id sem porta, blandit eros sed, pellentesque tortor. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus commodo risus nec ante consequat, vitae condimentum neque imperdiet. Quisque id pulvinar ante, imperdiet volutpat purus. Praesent viverra vulputate lacus a malesuada. Proin rhoncus sapien ut odio semper, vitae aliquet dui eleifend. Vestibulum at dignissim orci, nec vulputate neque. Donec semper quam sit amet eros blandit, quis sollicitudin nisi facilisis. In hendrerit tortor placerat nisl varius, eu scelerisque velit rutrum.</p>

	</main>

	<button class="standart-btn">Посмотреть весь каталог</button>
	<hr class="gradient">

<!-- Обобщающая информация о сайте -->

	<footer>
		<div class="wrapper">
			<p class="info">&copy; Bernigend, 2018</p>
		</div>
	</footer>

<!-- Поднятие наверх -->

	<div class="arrow-up" onclick="scrollUp ();"></div>

<!-- Корзина -->

	<div class="cart-place" id="js-cart">
		<div class="wrapper">
			<div class="cart"><strong>Корзина:</strong> 1 товар на сумму <strong>7'000 ₽</strong></div>
		</div>
	</div>

<!-- Подключение скриптов -->

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	<script src="javascripts/main-ui.js?6758"></script>

</body>
</html>