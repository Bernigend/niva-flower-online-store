<!--
 - Created by Bernigend', 'https://bernigend.ru || https://vk.com/bernigend
 - Created on 04/2018 for the project of the store
-->

<!DOCTYPE html>
<html lang="ru" dir="ltr">

<head>

	<meta charset="UTF-8">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<meta name="viewport" content="width=device-width', 'initial-scale=1.0">
	<meta name="description" content="Описание страницы короче 150 символов">

	<link href="https://fonts.googleapis.com/css?family=Open+Sans:300" rel="stylesheet">
	<link rel="stylesheet" href="../style/standart.css?<?php echo mt_rand (0, 99999); ?>">
	<link rel="stylesheet" href="../style/cpanel.css?<?php echo mt_rand (0, 99999); ?>">
	<link rel="icon" type="image/png" href="images/favicon.png">

	<title>Добавление товара - Панель управления</title>

</head>

<body>

<aside>
		
	<h2 class="title-section">Меню</h2>

	<div class="info-block">
		<span><strong>Admin</strong></span><br>
		<small><a href="/">Вернуться на сайт</a></small>
	</div>

	<nav>
		<ul>
			<li><a href="index.php">Главная</a></li>
			<li><a class="active" href="add-item.php">Добавление товара</a></li>
			<li><a href="?">Главная</a></li>
		</ul>
	</nav>

	<div class="footer-aside">
		<small><a href="?">Выйти</a></small>
	</div>

</aside>

<main>

	<h2 class="title-section">Добавление товара</h2>

	<form action="?add" method="POST" enctype="multipart/form-data">
		
		<div class="form-label">
			<label>
				<div class="input-title">Название товара (max. 64):</div>
				<input id="js-name-input" type="text" name="name-item" maxlength="64">
			</label>
		</div>

		<div class="form-label">
			<label>
				<div class="input-title">Стоимость (max. 11):</div>
				<input id="js-cost-input" type="text" name="cost-item" maxlength="11">
			</label>
		</div>

		<div class="form-label">
			<label>
				<div class="input-title">Краткое описание (max. 128):</div>
				<textarea id="js-mini-desc-input" name="mini-description-item" maxlength="128" cols="30" rows="2"></textarea>
			</label>
		</div>

		<div class="form-label">
			<div class="input-title">Тип товара:</div>
				<label><input id="js-radio-normal" type="radio" name="type-item" value="normal" checked> одиночный товар</label> <br>
				<label><input id="js-radio-set" type="radio" name="type-item" value="set"> набор</label>
		</div>

		<div class="form-label">
			<label>
				<div class="input-title">Полное описание (max ?)</div>
				<textarea name="description-item" cols="30" rows="10"></textarea>
			</label>
		</div>

		<div class="form-label">
			<label>
				<div class="input-title">Миниатюра:</div>
				<input id="js-mini-img-input" type="file" accept=".jpg, .jpeg, .png" name="mini_img">
			</label>
		</div>

		<div class="form-label">
			<label>
				<div class="input-title">Изображения:</div>
				<input type="file" accept=".jpg, .jpeg, .png" name="files[]" multiple>
			</label>
		</div>

		<input type="submit" value="Добавить">
		
	</form>

	<br><small>* все поля обязательны для заполнения</small>


	<div class="preview normal" style="display: none;">
		<h3>Предварительный просмотр:</h3> <br>
		<div class='product hot'>
			<div class='info'>
				<img class="js-img-item" src='/current-files/uploaded-images/default-product.png' alt='Изображение товара'><hr>
				<h3 class='js-name-item name'>Название товара</h3>
				<div class='js-cost-item cost'>10'990 ₽</div>
				<hr>
			</div>
			<button class='standart-btn'>В корзину</button>
		</div>
	</div>

	<div class="preview set" style="display: none;">
		<h3>Предварительный просмотр:</h3> <br>
		<div class='product set '>
			<div class='info'>
				<h3 class='js-name-item name'>Название набора</h3><hr>
				<img class="js-img-item" src='/current-files/uploaded-images/default-product.png' alt='Изображение товара Набор'>
				<p id="js-mini-desc-item">Краткое описание</p>
				<hr>
				<div class='js-cost-item cost'>10'990 ₽</div>
				<hr>
			</div>
			<button class='standart-btn'>В корзину</button>
		 </div>
	</div>

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

	<script>

		function fix_preview_block () {
			if ($("#js-name-input").outerWidth () + $(".preview.normal").outerWidth () > $("main").width ())
				$(".preview.normal").css ("position", "static")
			else
				$(".preview.normal").css ("position", "absolute")

			if ($("#js-name-input").outerWidth () + $(".preview.set").outerWidth () > $("main").width ())
				$(".preview.set").css ("position", "static")
			else
				$(".preview.set").css ("position", "absolute")

			$(".product").each (function (index, element) {
				$("button", element).css ("width", $("img", element).width ());
			});
		}

		function nmb (str) {
			return str.replace (/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1\'');
		}


		var preview_type = $("input[name='type-item']:checked").val (),

			input_name      = $("#js-name-input"),
			input_cost      = $("#js-cost-input"),
			input_mini_desc = $("#js-mini-desc-input"),
			input_mini_img  = $("#js-mini-img-input"),

			item_name      = $(".js-name-item"),
			item_cost      = $(".js-cost-item"),
			item_mini_desc = $("#js-mini-desc-item"),
			item_mini_img  = $(".js-img-item");


		$(document).ready (function () {
			
			$(".preview." + preview_type).css ("display", "block");

			input_name[0].oninput = function () {
				item_name[0].innerHTML = input_name[0].value;
				item_name[1].innerHTML = input_name[0].value;
			}

			input_cost[0].oninput = function () {
				item_cost[0].innerHTML = nmb (input_cost[0].value) + " ₽";
				item_cost[1].innerHTML = nmb (input_cost[0].value) + " ₽";
			}

			input_mini_desc[0].oninput = function () {
				item_mini_desc[0].innerHTML = nmb (input_mini_desc[0].value);
			}

			input_mini_img[0].addEventListener ("change", function () {
				if (this.files[0]) {
					var fr = new FileReader ();
					fr.addEventListener ("load", function () {
						item_mini_img[0].src = fr.result;
						item_mini_img[1].src = fr.result;
					}, false);
					fr.readAsDataURL (this.files[0]);
				}
			});

			$('input[type=radio][name=type-item]').change (function () {
				if ($(this).val() != preview_type) {
					switch($(this).val()) {
						case "set": 
							var old_preview_type = 'normal'
							break;
						case "normal": 
							var old_preview_type = 'set'
							break;
					}
					preview_type = $(this).val();
					$(".preview." + old_preview_type).css ("display", "none");
					$(".preview." + preview_type).css ("display", "block");
				}
			});

			fix_preview_block ();

		});


		$(window).resize (function () {
			fix_preview_block ();
		});

	</script>

</main>