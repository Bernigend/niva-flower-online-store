<!--
 - Created by Bernigend, https://bernigend.ru || https://vk.com/bernigend
 - Created on 03/2018 for the project of the store
-->

<!DOCTYPE html>
<html lang="ru" dir="ltr">

<head>

	<meta charset="UTF-8">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="Описание страницы короче 150 символов">

	<link href="https://fonts.googleapis.com/css?family=Open+Sans:300" rel="stylesheet">
	<link rel="stylesheet" href="style/standart.css?98776">
	<link rel="icon" type="image/png" href="images/favicon.png">

	<title>Интернет-магазин цветов в Сергиевом Посаде</title>

</head>

<body>

<!-- Заголовок страницы + навигация верхнего уровня -->

	<header>

		<div class="logo"></div>

		<nav id="js-navigation">
			<ul>
				<li><a class="active" href="?">Главная</a></li>
				<li><a href="about-us.php">О нас</a></li>
				<li><a href="catalog.php">Каталог</a></li>
				<li><a href="delivery.php">Доставка</a></li>
				<li><a href="contacts.php">Контакты</a></li>
			</ul>
		</nav>
		
	</header>

<!-- Главный слайдер -->
		
	<div class="slider">
		<div class="all-slides">
			<div class="slide" style="background-image: url(images/default-slide-1.png)"></div><div class="slide" style="background-image: url(images/default-slide-1.png)"></div><div class="slide" style="background-image: url(images/default-slide-1.png)"></div><div class="slide" style="background-image: url(images/default-slide-1.png)"></div>
		</div>
		<div class="next" onclick="sliderNext ();"></div>
		<div class="last" onclick="sliderLast ();"></div>
	</div>

<!-- Основной контент страницы -->

	<main class="wrapper">

		<h2 class="title">Наборы</h2>

		<section class="products">
			
			<div class="product set">
				<div class="info">
					<h3 class="name">Тестовый товар</h3><hr>
					<img src="images/default-product.png" alt="Изображение товара <Название товара>">
					<p>Описание набора, он такой классный, купите его - не пожалеете, честное пионерское, обещаю, будете прыгать от счастья :)</p>
					<hr>
					<div class="cost">7'000 ₽</div>
					<hr>
				</div>
				<button class="standart-btn">В корзину</button>
			</div>

			<div class="product set">
				<div class="info">
					<h3 class="name">Тестовый товар Тестовый товар Тестовый товар Тестовый товар</h3><hr>
					<img src="images/default-product.png" alt="Изображение товара <Название товара>">
					<p>Описание набора, он такой классный, купите его - не пожалеете, честное пионерское, обещаю, будете прыгать от счастья :), купите его - не пожалеете, честное пионерское, обещаю, будете прыгать от счастья :), купите его - не пожалеете, честное пионерское, обещаю, будете прыгать от счастья :)</p>
					<hr>
					<div class="cost">7'000 ₽</div>
					<hr>
				</div>
				<button class="standart-btn">В корзину</button>
			</div>

			<div class="product set">
				<div class="info">
					<h3 class="name">Тестовый товар</h3><hr>
					<img src="images/default-product.png" alt="Изображение товара <Название товара>">
					<p>Описание набора, он такой классный, купите его - не пожалеете, честное пионерское, обещаю, будете прыгать от счастья :)</p>
					<hr>
					<div class="cost">7'000 ₽</div>
					<hr>
				</div>
				<button class="standart-btn">В корзину</button>
			</div>

		</section>
	</main>

	<button class="standart-btn">Посмотреть весь каталог</button>
	<hr class="gradient">

<!-- Обобщающая информация о сайте -->

	<footer>
		<div class="wrapper">
			<p>&copy; Bernigend, 2018</p>
		</div>
	</footer>

<!-- Поднятие наверх -->

	<div class="arrow-up" onclick="scrollUp ();"></div>

<!-- Корзина -->

	<div class="cart-place" id="js-cart">
		<div class="wrapper">
			<div class="cart"><strong>Корзина:</strong> 1 товар на сумму <strong>7'000 ₽</strong></div>
		</div>
	</div>

<!-- Подключение скриптов -->

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	<script src="javascripts/main-ui.js?6758"></script>

</body>
</html>