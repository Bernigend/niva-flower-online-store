<?php

// подключение системных функций
	require_once ("system-core/main-functions.php");


// подключение шаблона шапки
	$system->set_active_page ('catalog');
	$system->set_title_of_page ('Каталог товаров');
	require_once ("system-includes/template-header.php");


// вывод обычных товаров

	// проверяем наличие товаров
	if (have_items (array ('type' => 'normal'))) {

		echo '<h2 class="title">Все товары</h2><section class="products">';
		// получаем данные для обработки в цикле
		$products = get_id_items (array ('type' => 'normal', 'order_by' => '`id`'));

		while ($product = $products->fetch_assoc ()) {

			// получаем объект продукта
			$product = new Product ($product ["id"]);
			$product->init ();

			echo "<div class='product {$product->getMoreClasses ()}'>
					<div class='info'>
						<img src='{$product->getUrlImg ()}' alt='Изображение товара {$product->getTitle ()}'><hr>
						<h3 class='name'>{$product->getTitle ()}</h3>
						<div class='cost'>{$product->getCost ()}</div>
						<hr>
					</div>
					<button class='standart-btn'>В корзину</button>
				 </div>";

		}
		echo '</section><hr>';

	}


// вывод наборов

	// проверяем наличие наборов
	if (have_items (array ('type' => 'set'))) {

		echo '<h2 class="title">Наборы</h2><section class="products">';
		// получаем данные для обработки в цикле
		$products = get_id_items (array ('type' => 'set', 'order_by' => 'RAND()'));

		while ($product = $products->fetch_assoc ()) {

			// получаем объект продукта
			$product = new Product ($product ["id"]);
			$product->init ();

			echo "<div class='product set {$product->getMoreClasses ()}'>
					<div class='info'>
						<h3 class='name'>{$product->getTitle ()}</h3><hr>
						<img src='{$product->getUrlImg ()}' alt='Изображение товара {$product->getTitle ()}'>
						<p>{$product->getMiniDescription ()}</p>
						<hr>
						<div class='cost'>{$product->getCost ()}</div>
						<hr>
					</div>
					<button class='standart-btn'>В корзину</button>
				 </div>";

		}

		echo '</section>';

	} else echo '<h2 class="title">Наборов нет</h2>';


// подключение шаблона футера
	require_once ("system-includes/template-footer.php");

?>