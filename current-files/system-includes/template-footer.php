	</main>

	<button class="standart-btn" onclick="location.href='catalog.php'">Посмотреть весь каталог</button>

<!-- Обобщающая информация о сайте -->

	<footer>
		<div class="wrapper">
			<p class="info">&copy; Bernigend, 2018 <br> <small>Генерация: <?php echo generationStop ($startTime); ?></small></p>
		</div>
	</footer>

<!-- Поднятие наверх -->

	<div class="arrow-up" onclick="scrollUp ();"></div>

<!-- Корзина -->

	<div class="cart-place" id="js-cart" data-id="123" onclick="openCart ()">
		<div class="wrapper">
			<div class="cart"><strong>Корзина:</strong> 2 товара на сумму <strong>12'500 ₽</strong></div>
		</div>
	</div>

	<!-- Открытая корзина -->

	<div class="cart-open-place" id="js-cart-open-place">
		<div class="wrapper">

			<small><div class="cart-close" onclick="closeCart ()">закрыть</div></small>
			<h2 class="title">Корзина</h2><hr>

			<section id="js-cart-items">
				<div class="cart-product">
					<span class="cart-name-item">Лазурный цветок</span>
					<div class="item-info">
						<span class="cart-item-count">1 шт, </span>
						<span class="cart-item-cost">7'000 ₽</span>
					</div>
				</div>
				<div class="cart-product">
					<span class="cart-name-item">Жёлтый цветок</span>
					<div class="item-info">
						<span class="cart-item-count">1 шт, </span>
						<span class="cart-item-cost">5'500 ₽</span>
					</div>
				</div>
			</section>

			<section class="display-none" id="js-user-info">
				<form action="<?php echo ABS_PATH; ?>/?" method="POST">
					<div class="form-label">
						<label>
							<div class="input-title">Ваше имя:</div>
							<input name="user-name" type="text">
						</label>
					</div>
					<div class="form-label">
						<label>
							<div class="input-title">Ваш e-mail:</div>
							<input name="user-mail" type="text">
						</label>
					</div>
					<div class="form-label">
						<label>
							<div class="input-title">Дата подготовки заказа:</div>
							<input name="user-date" type="text">
						</label>
					</div>
				</form>
			</section>

			<hr>
			<strong>Итого:</strong> <span id="js-cart-total">12'500 ₽</span>
		</div>
		<button onclick="executeCart ()" id="js-cart-items-btn">Оформить заказ</button>
		<button onclick="cancelCart ()" id="js-user-info-btn" class="display-none" >Подтвердить заказ</button>
	</div>
	<div class="cart-open-place-black" id="js-cart-open-place-black"></div>

<!-- Подключение скриптов -->

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	<script src="javascripts/main-ui.js?v14"></script>

</body>
</html>