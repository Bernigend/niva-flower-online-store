<aside>
		
	<h2 class="title-section">Меню</h2>

	<div class="info-block">
		<span><strong><?php echo $user->get_param ('login'); ?></strong></span><br>
		<small><a href="<?php echo ABS_PATH; ?>">Вернуться на сайт</a></small>
	</div>

	<nav>
		<ul>
			<li><a <?php echo ($system->get_active_page () == "index") ? 'class="active"' : ''; ?> href="index.php">Главная</a></li>
			<li><a <?php echo ($system->get_active_page () == "add-item") ? 'class="active"' : ''; ?> href="add-item.php">Добавление товара</a></li>
			<li><a <?php echo ($system->get_active_page () == "editor-items") ? 'class="active"' : ''; ?> href="editor-items.php">Редактор товаров</a></li>
		</ul>
	</nav>

	<div class="footer-aside">
		<small><a href="<?php echo ABS_PATH; ?>/control-panel?logOut">Выйти</a></small>
	</div>

</aside>