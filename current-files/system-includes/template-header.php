<!--
 - Created by Bernigend, https://bernigend.ru || https://vk.com/bernigend
 - Created on 03/2018 for the project of the store
-->

<!DOCTYPE html>
<html lang="ru" dir="ltr">

<head>

	<meta charset="UTF-8">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="Описание страницы короче 150 символов">

	<link href="https://fonts.googleapis.com/css?family=Open+Sans:300" rel="stylesheet">
	<link rel="stylesheet" href="style/standart.css?<?php echo mt_rand (0, 9999); ?>">
	<link rel="icon" type="image/png" href="images/favicon.png">

	<title><?php
		if (isset ($titlePage))
			$system->set_title_of_page ($titlePage);

		echo $system->get_title_of_page ();
	?></title>

	<!--[if IE]>
		<link rel="stylesheet" href="style/for-ie.css?<?php echo mt_rand (0, 9999); ?>">
	<![endif]-->

</head>

<body>

<!-- Заголовок страницы + навигация верхнего уровня -->

	<?php

		if ($user) {
			echo '<div class="welcome-admin" style="padding: 5px;color: #fff;background-color: #40d0f1;background-image: linear-gradient(to right, #40d0f1, #b300ad);text-align: center;font-size: 1rem;">Вы авторизованы как Администратор <br> <a style="color:#fff;font-size:0.9rem" href="control-panel">перейти в панель управления</a></div>';
		}

	?>

	<header>

		<div class="logo"></div>

		<nav id="js-navigation">
			<ul>
				<li><a <?php echo ($system->get_active_page () == "index") ? 'class="active"' : ''; ?> href="index.php">Главная</a></li>
				<li><a <?php echo ($system->get_active_page () == "about-us") ? 'class="active"' : ''; ?> href="about-us.php">О нас</a></li>
				<li><a <?php echo ($system->get_active_page () == "catalog") ? 'class="active"' : ''; ?> href="catalog.php">Каталог</a></li>
				<li><a <?php echo ($system->get_active_page () == "delivery") ? 'class="active"' : ''; ?> href="delivery.php">Доставка</a></li>
				<li><a <?php echo ($system->get_active_page () == "contacts") ? 'class="active"' : ''; ?> href="contacts.php">Контакты</a></li>
			</ul>
		</nav>
		
	</header>

	<?php if ($system->get_param ('slider_status') == 'on') { ?>

<!-- Главный слайдер -->
		
	<div class="slider">
		<div class="all-slides">
			<div class="slide" style="background-image: url(images/default-slide-1.png)"></div><div class="slide" style="background-image: url(images/default-slide-1.png)"></div><div class="slide" style="background-image: url(images/default-slide-1.png)"></div><div class="slide" style="background-image: url(images/default-slide-1.png)"></div>
		</div>
		<div class="next" onclick="sliderNext ();"></div>
		<div class="last" onclick="sliderLast ();"></div>
	</div>

	<?php } ?>

<!-- Основной контент страницы -->

	<main class="wrapper <?php echo ($system->get_active_page () == "about-us" || $system->get_active_page () == "delivery" || $system->get_active_page () == "contacts") ? 'with-backg with-mg-bt with-all-pd' : ''; ?>">