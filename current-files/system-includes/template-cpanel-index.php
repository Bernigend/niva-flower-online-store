<main>
		
	<h2 class="title-section">Панель управления</h2>

	<?php echo $system->get_notifications (); ?>

	<h3>Слайдер:</h3> <br>
	Состояние слайдера: <?php echo $system->get_param ('slider_status'); ?> <br>
	- <a href="?slider_toggle" style="color: inherit">сменить статус</a>

	<br><br>

	<h3>Страница "О нас"</h3>
	<form action="?edit-page=about_us" method="POST">
		<div class="form-label">
			<label>
				<div class="input-title">Текст страницы:</div>
				<textarea class="forTinyMCE" name="page-content" id="" cols="30" rows="10"><?php echo $system->decodeStr ($system->get_param ("page_about_us_content")); ?></textarea>
			</label>
		</div>
		<input type="submit" value="Изменить">
	</form>

	<br><br>

	<h3>Страница "Доставка"</h3>
	<form action="?edit-page=delivery" method="POST">
		<div class="form-label">
			<label>
				<div class="input-title">Текст страницы:</div>
				<textarea class="forTinyMCE" name="page-content" id="" cols="30" rows="10"><?php echo $system->decodeStr ($system->get_param ("page_delivery_content")); ?></textarea>
			</label>
		</div>
		<input type="submit" value="Изменить">
	</form>

	<br><br>

	<h3>Страница "Контакты"</h3>
	<form action="?edit-page=contacts" method="POST">
		<div class="form-label">
			<label>
				<div class="input-title">Текст страницы:</div>
				<textarea class="forTinyMCE" name="page-content" id="" cols="30" rows="10"><?php echo $system->decodeStr ($system->get_param ("page_contacts_content")); ?></textarea>
			</label>
		</div>
		<input type="submit" value="Изменить">
	</form>


	<?php $system->destroy_notifications (); ?>

</main>