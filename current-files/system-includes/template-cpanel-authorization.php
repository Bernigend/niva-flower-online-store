<div class="login-form" id="js-log-form">
	<div class="with-backg">
		<h2 class="title-form">Авторизация</h2>
		<form action="?logIn" method="POST">
			<label><div class="name-input">Логин:</div> <input type="text" name="login"></label>
			<label><div class="name-input">Пароль:</div> <input type="password" name="password"></label>
			<input type="submit" value="Войти">
		</form>
	</div>
	<a href="<?php echo ABS_PATH; ?>" class="login-href">Вернуться на главную</a>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

<script>
	$(document).ready(function() {
		$("#js-log-form").css ({"margin-left": -$("#js-log-form").outerWidth ()/2, "margin-top": -$("#js-log-form").outerHeight ()/2});	
	});
</script>