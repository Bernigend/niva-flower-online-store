<!--
 - Created by Bernigend', 'https://bernigend.ru || https://vk.com/bernigend
 - Created on 04/2018 for the project of the store
-->

<!DOCTYPE html>
<html lang="ru" dir="ltr">

<head>

	<meta charset="UTF-8">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="Описание страницы короче 150 символов">

	<link href="https://fonts.googleapis.com/css?family=Open+Sans:300" rel="stylesheet">
	<link rel="stylesheet" href="../style/standart.css?<?php echo mt_rand (0, 99999); ?>">
	<link rel="stylesheet" href="../style/cpanel.css?<?php echo mt_rand (0, 99999); ?>">
	<link rel="icon" type="image/png" href="images/favicon.png">

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	<script src="../javascripts/tinymce/tinymce.min.js"></script>
	<script>
		$(document).ready(function () {
			tinyMCE.init({
				selector: "textarea.forTinyMCE",
				language: "ru",
				plugins:  "link code",
				mode: "exact",
				extended_valid_elements: 'script[language|type|src]'
			});
		}); 
	</script>

	<title><?php echo $system->get_title_of_page ('cpanel'); ?></title>

</head>

<body>