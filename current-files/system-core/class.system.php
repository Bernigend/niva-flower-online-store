<?php

# класс системы

class System {

	private $title_of_page = NULL;
	private $active_page = NULL;


// Разбивает число на тысячные
	public function nmb (int $count) {
		return number_format ($count, 0, "'", "'");
	}


// Защита числа
	public function defendInt ($integer) {
		global $db;
		if (isset ($integer) AND is_numeric ($integer))
			return (int)$integer;
		else return false;
	}


// Защита строки
	public function defendStr ($string) {
		global $db;
		if (isset($string) AND !empty($string)) {
			return $db->real_escape_string(addslashes(htmlspecialchars($string)));
		} else return false;
	}


// Декодирование строки после метода defendStr
	public function decodeStr (string $string) {
		return stripslashes (htmlspecialchars_decode ($string));
	}


// проверка наличия товаров
	public function have_items (array $params = NULL) {
		global $db;

		$sql = '';

		if (is_array ($params)) {
			$sql = 'WHERE ';

			if (isset ($params ['type'])) {
				$sql .= "`type_of_item` = '{$params ['type']}' AND ";
			}

			if (isset ($params ['hot'])) {
				if ((bool) $params ['hot'] == true)
					$sql .= '`is_hot` = 1 AND ';
				else
					$sql .= '`is_hot` = 0 AND ';
			}

			if (isset ($params ['sale'])) {
				if ((bool) $params ['sale'] == true)
					$sql .= '`sale_time` > '.time ().' AND ';
				else
					$sql .= '`sale_time` <= '.time ().' AND ';
			}
			$sql .= '1';
		}

		if ($db->query ("SELECT `id` FROM `all_items` {$sql}")->num_rows > 0)
			return true;
		else
			return false;
	}


// получение ID товаров БЕЗ fetch_assoc
	public function get_id_items (array $params = NULL) {
		global $db;

		$sql = '';

		if (is_array ($params)) {
			$sql = 'WHERE ';

			if (isset ($params ['type'])) {
				$sql .= "`type_of_item` = '{$params ['type']}' AND ";
			}

			if (isset ($params ['hot'])) {
				if ((bool) $params ['hot'] == true)
					$sql .= '`is_hot` = 1 AND ';
				else
					$sql .= '`is_hot` = 0 AND ';
			}

			if (isset ($params ['sale'])) {
				if ((bool) $params ['sale'] == true)
					$sql .= '`sale_time` > '.time ().' AND ';
				else
					$sql .= '`sale_time` <= '.time ().' AND ';
			}
			$sql .= '1';

			if (isset ($params ['order_by'])) {
				$sql .= " ORDER BY {$params ['order_by']}";
			}

			if (isset ($params ['limit'])) {
				$sql .= ' LIMIT '.$params ['limit'];
			}
		}

		if ($db->query ("SELECT `id` FROM `all_items` {$sql}")->num_rows > 0)
			return $db->query ("SELECT `id` FROM `all_items` {$sql}");
		else
			return false;
	}


// получает заголовок страницы
	public function get_title_of_page (string $typeOfPage = NULL) {
		if (!is_null ($typeOfPage) && $typeOfPage == 'cpanel')
			if (!is_null ($this->title_of_page))
				return $this->title_of_page.' - Панель управления';
			else
				return 'Панель управления';
		else
			if (!is_null ($this->title_of_page))
				return $this->title_of_page.' - Интернет-магазин цветов в Сергиевом Посаде';
			else
				return 'Интернет-магазин цветов в Сергиевом Посаде';
	}


// получает текущую страницу
	public function get_active_page () {
		return $this->active_page;
	}


// устанавливает заголовок страницы
	public function set_title_of_page (string $title) {
		$this->title_of_page = $title;
	}


// устанавливает текущую страницу
	public function set_active_page (string $namePage) {
		$this->active_page = $namePage;
	}


// создание уведомлений
	public function create_notification (array $params) {

		if (!isset ($params ['text']))
			return false;
		else
			$text = $params ['text'];

		if (!isset ($params ['type']))
			$type = 'error';
		else
			$type = $params ['type'];

		if (!isset ($params ['title']))
			$title = '';
		else
			$title = "<span class='title-{$type}'>{$params ['title']}</span>";

		if (isset ($_SESSION ['notifications']))
			$key = count ($_SESSION ['notifications']) + 1;
		else
			$key = 1;

		$_SESSION ['notifications'][$key] = "<div class='{$type}'>{$title} <span class='text-{$type}'>{$text}</span></div>";

		return true;

	}

// вывод уведомлений
	public function get_notifications () {

		if (!isset ($_SESSION['notifications']))
			return false;

		$return = '';
		foreach ($_SESSION['notifications'] as $notification) {
			$return .= $notification;
		}

		return $return;
	}


// очитска уведомлений
	public function destroy_notifications () {
		$_SESSION ['notifications'] = NULL;
		return true;
	}


// получение настроек системы
	public function get_param ($param) {
		global $db;
		$return = $db->query ("SELECT `value` FROM `all_settings` WHERE `name` = '{$param}'")->fetch_assoc ();
		return $return ['value'];
	}

}

?>