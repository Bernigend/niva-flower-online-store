<?php

	session_start ();
	// $_SESSION ['notifications'] = array ();

# Основные функции системы

// Рассчёт генерации страницы - начало
	function generationStart () {
		$start_array = explode(" ", microtime ());
		return $start_array[1] + $start_array[0];
	}
	$startTime = generationStart (); 
	

// Рассчёт генерации страницы - остановка и вывод
	function generationStop ($startTime) {
		$end_array = explode(" ", microtime ());
		return round (($end_array[1] + $end_array[0]) - $startTime, 3).' сек';
	}


// подключение конфигурации
	require_once ("config-system.php");


// подключение к базе данных
	$db = new Mysqli (DB_HOST, DB_USER, DB_PASS, DB_BASE);

	// в случае ошибки подключения
	if ($db->connect_errno > 0)
		die ("Ошибка подключения к базе данных!");

	// кодировка базы
	$db->set_charset ("utf8");


// класс системы
	require_once ("class.system.php");
		$system = new System;

// класс отдельного продукта
	require_once ("class.product.php");

// класс отдельного пользователя
	require_once ("class.user.php");

	// инициализация авторизованного пользователя
		if (isset ($_COOKIE ["id"]))
			$userId = defendInt ($_COOKIE ["id"]);
		else
			$userId = false;

		if (isset ($_COOKIE ["password"]))
			$userPassword = defendStr ($_COOKIE ["password"]);
		else
			$userPassword = false;

		if ($userId && $userPassword) {
			$user = new User ($userId, $userPassword);
			if (!$user->init ())
				$user = false;
		} else $user = false;


// Разбивает число на тысячные
	function nmb (int $count) {
		global $system;
		return $system->nmb ($count);
	}


// Защита числа
	function defendInt (int $integer) {
		global $system;
		return $system->defendInt ($integer);
	}


// Защита строки
	function defendStr (string $string) {
		global $system;
		return $system->defendStr ($string);
	}


// проверка наличия товаров
	function have_items (array $params = NULL) {
		global $system;
		return $system->have_items ($params);
	}


// получение ID товаров БЕЗ fetch_assoc
	function get_id_items (array $params = NULL) {
		global $system;
		return $system->get_id_items ($params);
	}

?>