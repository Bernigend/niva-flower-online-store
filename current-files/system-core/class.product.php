<?php

class Product {

	public $id;
	private $allInfo;


	public function __construct (int $idProduct) {
		$this->id = $idProduct;
	}


	// проверка на существование товара, если мы в этом не уверены
	// и инициализация
	public function init () {
		global $db;

		if ($db->query ("SELECT `id` FROM `all_items` WHERE `id` = $this->id")->num_rows > 0)
			$this->allInfo = $db->query ("SELECT * FROM `all_items` WHERE `id` = $this->id")->fetch_assoc ();
		else
			return false;

		return true;
	}

	// ВСЕ функции на get необходимо использовать вместе с echo

	// получение стоимости товара в зависимости от наличия скидки
	public function getCost () {
		if ($this->allInfo ['sale_time'] > time ())
			return '<s>'.nmb ($this->allInfo ["cost"]).' ₽</s> '.nmb ($this->allInfo ["sale_cost"]).' ₽';
		else
			return nmb ($this->allInfo ["cost"]).' ₽';
	}

	// получение стоимости товара в зависимости от наличия скидки
	public function getCostNumber () {
		return $this->allInfo ["cost"];
	}

	// получение скидочной стоимости товара
	public function getSaleCostNumber () {
		return $this->allInfo ["sale_cost"];
	}


	public function getEndDateOfSale (string $format = NULL) {
		if ($this->allInfo ["sale_time"] == 0)
			return '0';

		if (!is_null ($format)) {
			return date ($format, $this->allInfo ["sale_time"]);
		} else {
			return date ('d.m.Y', $this->allInfo ["sale_time"]);
		}
	}

	public function getEndTimeOfSale (string $format = NULL) {
		if ($this->allInfo ["sale_time"] == 0)
			return '0:00';

		if (!is_null ($format)) {
			return date ($format, $this->allInfo ["sale_time"]);
		} else {
			return date ('G:i', $this->allInfo ["sale_time"]);
		}
	}


	// получение дополнительных классов (set, sale) для оформления карточки товара
	public function getMoreClasses () {
		if ($this->allInfo ["sale_time"] > time ())
			return 'sale';
		elseif ($this->allInfo ["is_hot"])
			return 'hot';
		return false;
	}


	// вывод на экран товара
	public function print (string $flag = NULL) {
		if ($this->getTypeOfItem () == 'normal') {
			echo "<div class='product {$flag}' data-id='{$this->id}' data-cost='{$this->getCostNumber ()}'>
					<div class='info'>
						<img src='{$this->getUrlImg ()}' alt='Изображение товара {$this->getTitle ()}'><hr>
						<h3 class='name'>{$this->getTitle ()}</h3>
						<div class='cost'>{$this->getCost ()}</div>
						<hr>
					</div>
					<button class='standart-btn' onclick='addToCart (this)'>В корзину</button>
				 </div>";
		} else {
			echo "<div class='product set {$this->getMoreClasses ()}' data-id='{$this->id}' data-cost='{$this->getCostNumber ()}'>
					<div class='info'>
						<h3 class='name'>{$this->getTitle ()}</h3><hr>
						<img src='{$this->getUrlImg ()}' alt='Изображение товара {$this->getTitle ()}'>
						<p>{$this->getMiniDescription ()}</p>
						<hr>
						<div class='cost'>{$this->getCost ()}</div>
						<hr>
					</div>
					<button class='standart-btn' onclick='addToCart (this)'>В корзину</button>
				 </div>";
		}
		return true;
	}


	// получение ссылки на картинку товара
	public function getUrlImg () {
		return ABS_PATH.'/'.UPLOAD_FOLDER.'/'.$this->allInfo ["mini_image_url"];
	}


	// получение название товара
	public function getTitle () {
		return $this->allInfo ["title"];
	}


	// получение мини описания
	public function getMiniDescription () {
		return $this->allInfo ["mini_description"];
	}


	// получение полного описания
	public function getDescription () {
		return $this->allInfo ["description"];
	}


	// получение типа товара
	public function getTypeOfItem () {
		return $this->allInfo ["type_of_item"];
	}

}

?>