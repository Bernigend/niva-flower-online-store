<?php

# Основная конфигурация системы

# Подключение к базе данных #
	define ("DB_HOST", "localhost"); // хост
	define ("DB_USER", "root");      // пользователь
	define ("DB_PASS", "");  // пароль
	define ("DB_BASE", "flower-shop");  // база данных

# Конфигурация корня сайта #
	# Если файлы сайта находятся в корне домена, оставляем пустым
	# Иначе вводим путь к файлам сайта относительно корня домена (без "/" в конце)
	define ("ABS_PATH", "/current-files");

# Папка для загрузки изображений
	define ("UPLOAD_FOLDER", "uploaded-images");

# Конфигурация вывода ошибок (error_reporting ())
	error_reporting (E_ALL);

?>