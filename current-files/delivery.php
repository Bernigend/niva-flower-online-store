<?php

// подключение системных функций
	require_once ("system-core/main-functions.php");


// подключение шаблона шапки

	$system->set_active_page ('delivery');
	$system->set_title_of_page ('Доставка');
	require_once ("system-includes/template-header.php");

	echo $system->decodeStr ($system->get_param ('page_delivery_content'));

// подключение шаблона футера
	require_once ("system-includes/template-footer.php");

?>