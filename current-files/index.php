<?php

// подключение системных функций
	require_once ("system-core/main-functions.php");


// подключение шаблона шапки

	$system->set_active_page ('index');
	$system->set_title_of_page ('Главная');
	require_once ("system-includes/template-header.php");

// вывод популярных товаров
	if (have_items (array ('type' => 'normal', 'hot' => true))) {

		echo '<h2 class="title">Популярное</h2><section class="products">';
		$products = get_id_items (array ('type' => 'normal', 'hot' => true, 'order_by' => 'RAND()', 'limit' => 3));

		while ($product = $products->fetch_assoc ()) {

			$product = new Product ($product ["id"]);
			$product->init ();
			$product->print ('hot');

		}
		echo '</section><hr>';

	}


// вывод новых товаров
	if (have_items (array ('type' => 'normal'))) {

		echo '<h2 class="title">Новинки</h2><section class="products">';
		$products = get_id_items (array ('type' => 'normal', 'order_by' => '`date_add` DESC', 'limit' => 3));

		while ($product = $products->fetch_assoc ()) {

			$product = new Product ($product ["id"]);
			$product->init ();
			$product->print ('new');

		}
		echo '</section><hr>';

	}


// вывод скидочных товаров
	if (have_items (array ('type' => 'normal', 'sale' => true))) {

		echo '<h2 class="title">Скидки</h2><section class="products">';
		$products = get_id_items (array ('type' => 'normal', 'sale' => true, 'order_by' => 'RAND()', 'limit' => 3));

		while ($product = $products->fetch_assoc ()) {

			$product = new Product ($product ["id"]);
			$product->init ();
			$product->print ('sale');

		}
		echo '</section><hr>';

	}


// вывод наборов
	if (have_items (array ('type' => 'set'))) {

		echo '<h2 class="title">Наборы</h2><section class="products">';
		$products = get_id_items (array ('type' => 'set', 'order_by' => 'RAND()', 'limit' => 4));

		while ($product = $products->fetch_assoc ()) {
			
			$product = new Product ($product ["id"]);
			$product->init ();
			$product->print ();

		}

		echo '</section>';

	} else echo '<h2 class="title">Наборов нет</h2>';


// подключение шаблона футера
	require_once ("system-includes/template-footer.php");

?>