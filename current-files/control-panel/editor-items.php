<?php

	require_once '../system-core/main-functions.php';

	if (!$user) {
		header ('location: '.ABS_PATH);
		exit ();
	}

	$system->set_active_page ('editor-items');
	$system->set_title_of_page ('Редактор товаров');
	require_once '../system-includes/template-cpanel-header.php';
	require_once '../system-includes/template-cpanel-aside.php';

?>

<main>

	<h2 class="title-section">Редактор товаров</h2>

	<?php echo $system->get_notifications (); ?>

	<?php

	if (have_items ()) :
		$products = get_id_items ();

		while ($product = $products->fetch_assoc ()) {
			$product = new Product ($product ["id"]);
			$product->init ();
	
			echo '<div class="item">
				
				<img src="'.$product->getUrlImg ().'" alt="" class="item-img">
				<div class="item-info">
					<div><span class="item-category">Название:</span> <span>'.$product->getTitle ().'</span></div>
					<div><span class="item-category">Цена:</span> <span>'.$product->getCost ().'</span></div>
					<div><span class="item-category">Тип:</span> <span>'.(($product->getTypeOfItem () == 'normal') ? 'одиночный товар' : 'набор').'</span></div>
					<div><span class="item-category">Мини-описание:</span> <span>'.$product->getMiniDescription ().'</span></div>
					<div><span class="item-category">Полное описание:</span> <span>'.$product->getDescription ().'</span></div>
					
					<button onclick="location.href=\'edit-item.php?edit='.$product->id.'\'">Редактировать</button>
				</div>

			</div>';

		}

	else :

		echo 'Вы не создали ни одного товара';

	endif;

	$system->destroy_notifications ();

	?>

</main>