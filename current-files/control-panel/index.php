<?php

# подключение основных функций сайта
	require_once ("../system-core/main-functions.php");

# Авторизация пользователя
	if (isset ($_GET ["logIn"]) && !$user) {
		$login = defendStr ($_POST ["login"]);
		$password = defendStr ($_POST ["password"]);

		if (!$login || !$password) {
			echo "Ошибка входа!";
			exit;
		}

		$password = md5 (md5 ($password));

		if ($db->query ("SELECT `id` FROM `all_users` WHERE `login` = '{$login}' AND `password` = '{$password}' LIMIT 1")->num_rows == 0) {
			echo "User not found";
			exit;
		} else $user = $db->query ("SELECT `id` FROM `all_users` WHERE `login` = '{$login}' AND `password` = '{$password}' LIMIT 1")->fetch_assoc ();

		setcookie ('id', $user ["id"], time() + 86000, "/");
		setcookie ('password', $password, time() + 86000, "/");
		header ('location: ?');
		exit;
	}

# Выход пользователя
	if (isset ($_GET ["logOut"]) && $user) {
		setcookie ('id', "", time() - 86000, "/");
		setcookie ('password', "", time() - 86000, "/");
		header ('location: ?');
		exit;
	}

# Вывод авторизации
	if (!$user) {
		$system->set_title_of_page ('Авторизация');
		require_once ('../system-includes/template-cpanel-header.php');
		require_once ('../system-includes/template-cpanel-authorization.php');
		exit;
	}


# Редактирование настроек системы
	if (isset ($_GET ['slider_toggle'])) {
		if ($system->get_param ('slider_status') == 'on')
			$db->query ("UPDATE `all_settings` SET `value` = 'off' WHERE `name` = 'slider_status'");
		else
			$db->query ("UPDATE `all_settings` SET `value` = 'on' WHERE `name` = 'slider_status'");
		header ('location: ?');
		exit;
	}


# Редактирование контента отдельных страниц
	if (isset ($_GET ['edit-page']) && isset ($_POST ['page-content'])) {
		if ($_GET ['edit-page'] == 'about_us' && $system->defendStr ($_POST ['page-content'])) {
			$db->query ("UPDATE `all_settings` SET `value` = '{$system->defendStr ($_POST ['page-content'])}' WHERE `name` = 'page_about_us_content'");
			$system->create_notification (array ("text" => 'Успешно изменена страница "О нас"', "type" => 'success'));
		}

		if ($_GET ['edit-page'] == 'contacts' && $system->defendStr ($_POST ['page-content'])) {
			$db->query ("UPDATE `all_settings` SET `value` = '{$system->defendStr ($_POST ['page-content'])}' WHERE `name` = 'page_contacts_content'");
			$system->create_notification (array ("text" => 'Успешно изменена страница "Контакты"', "type" => 'success'));
		}

		if ($_GET ['edit-page'] == 'delivery' && $system->defendStr ($_POST ['page-content'])) {
			$db->query ("UPDATE `all_settings` SET `value` = '{$system->defendStr ($_POST ['page-content'])}' WHERE `name` = 'page_delivery_content'");
			$system->create_notification (array ("text" => 'Успешно изменена страница "Доставка"', "type" => 'success'));
		}

		header ('location: ?');
		exit;
	}

# Вывод главной страницы панели управления
	$system->set_title_of_page ('Главная');
	$system->set_active_page ('index');
	
	require_once ('../system-includes/template-cpanel-header.php');
	require_once ('../system-includes/template-cpanel-aside.php');
	require_once ('../system-includes/template-cpanel-index.php');

?>