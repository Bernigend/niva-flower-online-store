<?php

// подключение основных функций сайта
	require_once ("../system-core/main-functions.php");

	if (!$user) {
		header ('location: '.ABS_PATH);
		exit;
	}

# Добавление товара
	if (isset ($_GET ["add"])) {

		# Если заполнены не все поля
		if (!isset ($_POST ["name-item"]) || !isset ($_POST ["cost-item"]) || !isset ($_POST ["mini-description-item"]) ||
			!isset ($_POST ["type-item"]) || !isset ($_POST ["description-item"]) ||
			!isset ($_FILES ['files']) || !isset ($_FILES ['mini_img'])) {
			$system->create_notification (array ('title' => 'Ошибка!', 'text' => 'Проверьте правильность введенных данных.'));
			header ('location: ?');
			exit;
		}

		$title           = $system->defendStr ($_POST ["name-item"]);
		$cost            = $system->defendInt ($_POST ["cost-item"]);
		$miniDescription = $system->defendStr ($_POST ["mini-description-item"]);
		$typeItem        = $system->defendStr ($_POST ["type-item"]);
		$description     = $system->defendStr ($_POST ["description-item"]);

		# Проверка на корректность введенных данных
		if (!$title || !$cost || !$miniDescription || !$typeItem || !$description) {
			$system->create_notification (array ('title' => 'Ошибка!', 'text' => 'Проверьте правильность введенных данных.'));
			header ('location: ?');
			exit;
		}

		# Папка загрузки изображений
		$uploadFolder = UPLOAD_FOLDER;

		# Директория загрузки изображений
		$uploadDir = $_SERVER['DOCUMENT_ROOT'].ABS_PATH.'/'.$uploadFolder.'/';

		# Директория изображений для бд
		$uploadDirDB = '/'.$uploadFolder.'/';

		# Допустимые типы изображений и размер в байтах
		$types = array ('image/gif', 'image/png', 'image/jpeg');
		$size  = 10485760;


		# Загрузка файлов мультизагрузки
		foreach ($_FILES['files']['tmp_name'] as $key => $value) {

			# Проверка на загрузку через http
			if (!is_uploaded_file ($_FILES['files']['tmp_name'][$key])) {
				$system->create_notification (array (
					'title' => 'Ошибка!',
					'text'  => 'Файл загружен неверно! ['.basename ($_FILES['files']['name'][$key]).']'
				));
				header ('location: ?');
				exit;
			}

			# Проверка на тип файла
			if (!in_array (mime_content_type ($_FILES['files']['tmp_name'][$key]), $types)) {
				$system->create_notification (array (
					'title' => 'Ошибка!',
					'text' => mime_content_type ($_FILES['files']['tmp_name'][$key]).' Недопустимый формат файла! Разрешено: gif, png, jpeg. ['.basename ($_FILES['files']['name'][$key]).']'
				));
				header ('location: ?');
				exit;
			}

			# Проверка на размер файла
			if ($_FILES['files']['size'][$key] > $size) {
				$system->create_notification (array (
					'title' => 'Ошибка!',
					'text' => 'Размер файла превышает '.$size.' байт. ['.basename ($_FILES['files']['name'][$key]).']'
				));
				header ('location: ?');
				exit;
			}

			# Загружаемый файл
			$uploadFile = $uploadDir.time().'-'.basename ($_FILES['files']['name'][$key]);
			$uploadFileDB = $uploadDirDB.time().'-'.basename ($_FILES['files']['name'][$key]);

			# Загрузка файла
			if (!move_uploaded_file ($_FILES['files']['tmp_name'][$key], $uploadFile)) {
				$system->create_notification (array (
					'title' => 'Ошибка!',
					'text' => 'Файл '.$_FILES['files']['name'][$key].' не смог загрузиться [big_img].'
				));
				header ('location: ?');
				exit;
			}

			# Массив с url изображений
			$bigImages [$key] = $uploadFileDB;

		}

		# Название маленького изображения
		$nameMiniImg = time().'-'.basename ($_FILES['mini_img']['name']);

		# Путь загрузки маленького изображения
		$uploadFile = $uploadDir.$nameMiniImg;

		# Проверка на загрузку через http
		if (!is_uploaded_file ($_FILES['mini_img']['tmp_name'])) {
			$system->create_notification (array (
				'title' => 'Ошибка!',
				'text' => 'Файл загружен неверно! ['.basename ($_FILES['mini_img']['name']).']'
			));
			header ('location: ?');
			exit;
		}

		# Проверка на тип файла
		if (!in_array (mime_content_type ($_FILES['mini_img']['tmp_name']), $types)) {
			$system->create_notification (array (
				'title' => 'Ошибка!',
				'text' => 'Недопустимый формат файла! Разрешено: gif, png, jpeg. ['.basename ($_FILES['mini_img']['name']).']'
			));
			header ('location: ?');
			exit;
		}

		# Проверка на размер файла
		if ($_FILES['mini_img']['size'] > $size) {
			$system->create_notification (array (
				'title' => 'Ошибка!',
				'text' => 'Размер файла превышает '.$size.' байт. ['.basename ($_FILES['mini_img']['name']).']'
			));
			header ('location: ?');
			exit;
		}

		# Загрузка файла
		if (!move_uploaded_file ($_FILES['mini_img']['tmp_name'], $uploadFile)) {
			$system->create_notification (array (
				'title' => 'Ошибка!',
				'text' => 'Файл '.$_FILES['mini_img']['name'].' не смог загрузиться [mini_img]'
			));
			header ('location: ?');
			exit;
		}

		# Добавление товара в базу
		$db->query ("INSERT INTO `all_items` SET `title` = '{$title}',
												 `cost` = '{$cost}',
												 `mini_image_url` = '{$nameMiniImg}',
												 `type_of_item` = '{$typeItem}',
												 `mini_description` = '{$miniDescription}',
												 `description` = '{$description}',
												 `date_add` = ".time()) or die ($db->error);

		# ID добавленного товара
		$idItem = $db->insert_id;

		# Добавление в базу данных информации об остальных изображениях
		foreach ($bigImages as $key => $url) {
			$db->query ("INSERT INTO `all_images` SET `id_item` = {$idItem}, `url` = '{$url}'") or die ($db->error);
		}

		$system->create_notification (array (
			'title' => 'Успешно!',
			'text' => 'Новый товар ['.$title.'] был успешно добавлен!',
			'type' => 'success'
		));
		header ('location: ?');
		exit;

	}

	# Подключение шаблонов
	$system->set_title_of_page ('Добавление товара');
	$system->set_active_page ('add-item');
	
	require_once ('../system-includes/template-cpanel-header.php');
	require_once ('../system-includes/template-cpanel-aside.php');

?>

<main>

<h2 class="title-section">Добавление товара</h2>

	<?php echo $system->get_notifications (); ?>

	<form action="?add" method="POST" enctype="multipart/form-data">
		
		<div class="form-label">
			<label>
				<div class="input-title">Название товара:</div>
				<input id="js-name-input" type="text" name="name-item" maxlength="64">
			</label>
			<span><small><i>Название товара не должно превышать 64 символа</i></small></span>
		</div>

		<div class="form-label">
			<label>
				<div class="input-title">Стоимость:</div>
				<input id="js-cost-input" type="text" name="cost-item" maxlength="11">
			</label>
			<span><small><i>Цена товара не должна превышать 11 символов</i></small></span>
		</div>

		<div class="form-label">
			<label>
				<div class="input-title">Краткое описание:</div>
				<textarea id="js-mini-desc-input" name="mini-description-item" maxlength="256" cols="30" rows="2"></textarea>
			</label>
			<span><small><i>Краткое описание товара не должно превышать 256 символов</i></small></span>
		</div>

		<div class="form-label">
			<div class="input-title">Тип товара:</div>
				<label><input id="js-radio-normal" type="radio" name="type-item" value="normal" checked> одиночный товар</label> <br>
				<label><input id="js-radio-set" type="radio" name="type-item" value="set"> набор</label>
		</div>

		<div class="form-label">
			<label>
				<div class="input-title">Полное описание:</div>
				<textarea name="description-item" class="forTinyMCE" cols="30" rows="10"></textarea>
			</label>
		</div>

		<div class="form-label">
			<label>
				<div class="input-title">Миниатюра:</div>
				<input id="js-mini-img-input" type="file" accept=".jpg, .jpeg, .png" name="mini_img">
			</label> <br>
			<span><small><i>Чтобы карточки товаров выглядели одинаково, все миниатюры должны быть одного размера.</span></i></small></span> <br>
			<span><small><i>Рекомендуемый размер: 350px X 350px (ширина X высота)</span></i></small></span>
		</div>

		<div class="form-label">
			<label>
				<div class="input-title">Изображения:</div>
				<input type="file" accept=".jpg, .jpeg, .png" name="files[]" multiple>
			</label>
		</div>

		<input type="submit" value="Добавить">
		
	</form>

	<br><small>* все поля обязательны для заполнения</small>


	<div class="preview normal" style="display: none;">
		<h3>Предварительный просмотр:</h3> <br>
		<div class='product hot'>
			<div class='info'>
				<img class="js-img-item" src='<?php echo ABS_PATH; ?>/images/default-product.png' alt='Изображение товара'><hr>
				<h3 class='js-name-item name'>Название товара</h3>
				<div class='js-cost-item cost'>10'990 ₽</div>
				<hr>
			</div>
			<button class='standart-btn'>В корзину</button>
		</div>
	</div>

	<div class="preview set" style="display: none;">
		<h3>Предварительный просмотр:</h3> <br>
		<div class='product set '>
			<div class='info'>
				<h3 class='js-name-item name'>Название набора</h3><hr>
				<img class="js-img-item" src='<?php echo ABS_PATH; ?>/images/default-product.png' alt='Изображение товара Набор'>
				<p id="js-mini-desc-item">Краткое описание</p>
				<hr>
				<div class='js-cost-item cost'>10'990 ₽</div>
				<hr>
			</div>
			<button class='standart-btn'>В корзину</button>
		 </div>
	</div>

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

	<script>

		function fix_preview_block () {
			if ($("#js-name-input").outerWidth () + $(".preview.normal").outerWidth () > $("main").width ())
				$(".preview.normal").css ("position", "static")
			else
				$(".preview.normal").css ("position", "fixed")

			if ($("#js-name-input").outerWidth () + $(".preview.set").outerWidth () > $("main").width ())
				$(".preview.set").css ("position", "static")
			else
				$(".preview.set").css ("position", "fixed")

			$(".preview.normal button").css ("width", $(".preview.normal img").width ());
		}

		function nmb (str) {
			return str.replace (/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1\'');
		}


		var preview_type = $("input[name='type-item']:checked").val (),

			input_name      = $("#js-name-input"),
			input_cost      = $("#js-cost-input"),
			input_mini_desc = $("#js-mini-desc-input"),
			input_mini_img  = $("#js-mini-img-input"),

			item_name      = $(".js-name-item"),
			item_cost      = $(".js-cost-item"),
			item_mini_desc = $("#js-mini-desc-item"),
			item_mini_img  = $(".js-img-item");


		$(document).ready (function () {
			
			$(".preview." + preview_type).css ("display", "block");

			input_name[0].oninput = function () {
				item_name[0].innerHTML = input_name[0].value;
				item_name[1].innerHTML = input_name[0].value;
			}

			input_cost[0].oninput = function () {
				item_cost[0].innerHTML = nmb (input_cost[0].value) + " ₽";
				item_cost[1].innerHTML = nmb (input_cost[0].value) + " ₽";
			}

			input_mini_desc[0].oninput = function () {
				item_mini_desc[0].innerHTML = nmb (input_mini_desc[0].value);
			}

			input_mini_img[0].addEventListener ("change", function () {
				if (this.files[0]) {
					var fr = new FileReader ();
					fr.addEventListener ("load", function () {
						item_mini_img[0].src = fr.result;
						item_mini_img[1].src = fr.result;
					}, false);
					fr.readAsDataURL (this.files[0]);
				}
			});

			$('input[type=radio][name=type-item]').change (function () {
				if ($(this).val() != preview_type) {
					switch($(this).val()) {
						case "set": 
							var old_preview_type = 'normal'
							break;
						case "normal": 
							var old_preview_type = 'set'
							break;
					}
					preview_type = $(this).val();
					$(".preview." + old_preview_type).css ("display", "none");
					$(".preview." + preview_type).css ("display", "block");
				}
				$(".preview.normal button").css ("width", $(".preview.normal img").width ());
			});

			fix_preview_block ();

		});


		$(window).resize (function () {
			fix_preview_block ();
		});

	</script>

</main>

<?php $system->destroy_notifications (); ?>