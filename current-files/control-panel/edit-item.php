<?php

	require_once '../system-core/main-functions.php';

	if (!$user) {
		header ('location: '.ABS_PATH);
		exit ();
	}


	if (!isset ($_GET ['edit'])) {
		header ('location: editor-items.php');
		exit ();
	}


	$id_product = $system->defendInt ($_GET ['edit']);

	if (!$id_product) {
		$system->create_notification (array (
			'title' => 'Ошибка!',
			'text'  => 'Передан неверный идентификатор товара.'
		));
		header ('location: editor-items.php');
		exit ();
	}


	$product = new Product ($id_product);
	
	if (!$product->init ()) {
		$system->create_notification (array (
			'title' => 'Ошибка!',
			'text'  => 'Товар не был найден.'
		));
		header ('location: editor-items.php');
		exit ();
	}


	if (isset ($_GET ['cancel'])) {

		# Если заполнены не все поля
		if (!isset ($_POST ["name-item"]) || !isset ($_POST ["cost-item"]) || !isset ($_POST ["mini-description-item"]) ||
			!isset ($_POST ["type-item"]) || !isset ($_POST ["description-item"])) {
			$system->create_notification (array ('title' => 'Ошибка!', 'text' => 'Проверьте правильность введенных данных.'));
			header ('location: ?edit='.$product->id);
			exit;
		}

		$title           = $system->defendStr ($_POST ["name-item"]);
		$cost            = $system->defendInt ($_POST ["cost-item"]);
		$typeItem        = $system->defendStr ($_POST ["type-item"]);
		$miniDescription = $system->defendStr ($_POST ["mini-description-item"]);
		$description     = $system->defendStr ($_POST ["description-item"]);

		# Проверка данных скидки товара
		if (isset ($_POST ["sale-cost-item"]) && !empty ($_POST ["sale-cost-item"]) ||
			isset ($_POST ["sale-date-item"]) && !empty ($_POST ["sale-date-item"]) ||
			isset ($_POST ["sale-time-item"]) && !empty ($_POST ["sale-time-item"]) && $_POST ["sale-time-item"] != '0:00') {

			if (!isset ($_POST ["sale-cost-item"]) || !$system->defendInt ($_POST ["sale-cost-item"])) {
				$system->create_notification (array ('title' => 'Ошибка!', 'text' => 'Не указана цена со скидкой.'));
				header ('location: ?edit='.$product->id);
				exit;
			}

			if (!isset ($_POST ["sale-date-item"]) || !$system->defendStr ($_POST ["sale-date-item"])) {
				$system->create_notification (array ('title' => 'Ошибка!', 'text' => 'Не указана дата окончания скидки.'));
				header ('location: ?edit='.$product->id);
				exit;
			}

			if (!isset ($_POST ["sale-time-item"]) || !$system->defendStr ($_POST ["sale-time-item"])) {
				$system->create_notification (array ('title' => 'Ошибка!', 'text' => 'Не указано время окончания скидки.'));
				header ('location: ?edit='.$product->id);
				exit;
			}

			$saleCost = $system->defendInt ($_POST ["sale-cost-item"]);
			$saleDate = $system->defendStr ($_POST ["sale-date-item"]);
			$saleTime = $system->defendStr ($_POST ["sale-time-item"]);

			# проверка корректности даты
			$saleDateExploded = explode ('.', $saleDate);
			if (!checkdate ((int)$saleDateExploded [1], (int)$saleDateExploded [0], (int)$saleDateExploded [2])) {
				$system->create_notification (array ('title' => 'Ошибка!', 'text' => 'Дата окончания скидки введена неверно.'));
				header ('location: ?edit='.$product->id);
				exit;
			}

			$saleDate = strtotime ($saleDate);

			$saleTimeExploded = explode (':', $saleTime);
			if (count ($saleTimeExploded) == 2) {
				if ((int)$saleTimeExploded [0] < 0 || (int)$saleTimeExploded [0] > 24 ||
					(int)$saleTimeExploded [1] < 0 || (int)$saleTimeExploded [1] > 59) {
					$system->create_notification (array ('title' => 'Ошибка!', 'text' => 'Время окончания скидки введено неверно.'));
					header ('location: ?edit='.$product->id);
					exit;
				}
			} else {
				$system->create_notification (array ('title' => 'Ошибка!', 'text' => 'Время окончания скидки введено неверно.'));
				header ('location: ?edit='.$product->id);
				exit;
			}

			$saleDate += (int)$saleTimeExploded [0] * 3600 + (int)$saleTimeExploded [1] * 60;

		} else {
			$saleCost = 0;
			$saleDate = 0;
			$saleTime = 0;
		}

		# Проверка на корректность введенных данных
		if (!$title || !$cost || !$miniDescription || !$typeItem || !$description) {
			$system->create_notification (array ('title' => 'Ошибка!', 'text' => 'Проверьте правильность введенных данных.'));
			header ('location: ?edit='.$product->id);
			exit;
		}

		# Папка загрузки изображений
		$uploadFolder = UPLOAD_FOLDER;

		# Директория загрузки изображений
		$uploadDir = $_SERVER['DOCUMENT_ROOT'].ABS_PATH.'/'.$uploadFolder.'/';

		# Директория изображений для бд
		$uploadDirDB = '/'.$uploadFolder.'/';

		# Допустимые типы изображений и размер в байтах
		$types = array ('image/gif', 'image/png', 'image/jpeg');
		$size  = 10485760;

		if (isset ($_FILES ['files']) && !in_array (0, $_FILES ['files']['size'])) {

			# Загрузка файлов мультизагрузки
			foreach ($_FILES['files']['tmp_name'] as $key => $value) {

				# Проверка на загрузку через http
				if (!is_uploaded_file ($_FILES['files']['tmp_name'][$key])) {
					$system->create_notification (array (
						'title' => 'Ошибка!',
						'text'  => 'Файл загружен неверно! ['.basename ($_FILES['files']['name'][$key]).', big]'
					));
					header ('location: ?edit='.$product->id);
					exit;
				}

				# Проверка на тип файла
				if (!in_array (mime_content_type ($_FILES['files']['tmp_name'][$key]), $types)) {
					$system->create_notification (array (
						'title' => 'Ошибка!',
						'text' => mime_content_type ($_FILES['files']['tmp_name'][$key]).' Недопустимый формат файла! Разрешено: gif, png, jpeg. ['.basename ($_FILES['files']['name'][$key]).']'
					));
					header ('location: ?edit='.$product->id);
					exit;
				}

				# Проверка на размер файла
				if ($_FILES['files']['size'][$key] > $size) {
					$system->create_notification (array (
						'title' => 'Ошибка!',
						'text' => 'Размер файла превышает '.$size.' байт. ['.basename ($_FILES['files']['name'][$key]).']'
					));
					header ('location: ?edit='.$product->id);
					exit;
				}

				# Загружаемый файл
				$uploadFile = $uploadDir.time().'-'.basename ($_FILES['files']['name'][$key]);
				$uploadFileDB = $uploadDirDB.time().'-'.basename ($_FILES['files']['name'][$key]);

				# Загрузка файла
				if (!move_uploaded_file ($_FILES['files']['tmp_name'][$key], $uploadFile)) {
					$system->create_notification (array (
						'title' => 'Ошибка!',
						'text' => 'Файл '.$_FILES['files']['name'][$key].' не смог загрузиться [big_img].'
					));
					header ('location: ?edit='.$product->id);
					exit;
				}

				# Массив с url изображений
				$bigImages [$key] = $uploadFileDB;

			}

		}

		if (isset ($_FILES ['mini_img']) && $_FILES ['mini_img']['size'] > 0) {

			# Название маленького изображения
			$nameMiniImg = time().'-'.basename ($_FILES['mini_img']['name']);

			# Путь загрузки маленького изображения
			$uploadFile = $uploadDir.$nameMiniImg;

			# Проверка на загрузку через http
			if (!is_uploaded_file ($_FILES['mini_img']['tmp_name'])) {
				$system->create_notification (array (
					'title' => 'Ошибка!',
					'text' => 'Файл загружен неверно! ['.basename ($_FILES['mini_img']['name']).', mini]'
				));
				header ('location: ?edit='.$product->id);
				exit;
			}

			# Проверка на тип файла
			if (!in_array (mime_content_type ($_FILES['mini_img']['tmp_name']), $types)) {
				$system->create_notification (array (
					'title' => 'Ошибка!',
					'text' => 'Недопустимый формат файла! Разрешено: gif, png, jpeg. ['.basename ($_FILES['mini_img']['name']).']'
				));
				header ('location: ?edit='.$product->id);
				exit;
			}

			# Проверка на размер файла
			if ($_FILES['mini_img']['size'] > $size) {
				$system->create_notification (array (
					'title' => 'Ошибка!',
					'text' => 'Размер файла превышает '.$size.' байт. ['.basename ($_FILES['mini_img']['name']).']'
				));
				header ('location: ?edit='.$product->id);
				exit;
			}

			# Загрузка файла
			if (!move_uploaded_file ($_FILES['mini_img']['tmp_name'], $uploadFile)) {
				$system->create_notification (array (
					'title' => 'Ошибка!',
					'text' => 'Файл '.$_FILES['mini_img']['name'].' не смог загрузиться [mini_img]'
				));
				header ('location: ?edit='.$product->id);
				exit;
			}

			$db->query ("UPDATE `all_items` SET `mini_image_url` = '{$nameMiniImg}' WHERE `id` = '{$product->id}'");

			unlink ($product->getUrlImg ());

		}

		# Добавление товара в базу
		$db->query ("UPDATE `all_items` SET `title` = '{$title}',
											`cost` = '{$cost}',
											`type_of_item` = '{$typeItem}',
											`mini_description` = '{$miniDescription}',
											`description` = '{$description}',
											`sale_cost` = '{$saleCost}',
											`sale_time` = '{$saleDate}'
					WHERE `id` = '{$product->id}'") or die ($db->error);

		# Добавление в базу данных информации об остальных изображениях
		if (isset ($_FILES ['files']) && !in_array (0, $_FILES ['files']['size'])) {
			foreach ($bigImages as $key => $url) {
				$db->query ("INSERT INTO `all_images` SET `id_item` = '{$product->id}', `url` = '{$url}'") or die ($db->error);
			}
		}

		$system->create_notification (array (
			'title' => 'Успешно!',
			'text' => 'Товар ['.$title.'] был успешно отредактирован!',
			'type' => 'success'
		));
		header ('location: ?edit='.$product->id);

		exit;
	}


	$system->set_active_page ('editor-items');
	$system->set_title_of_page ('Редактирование товара');
	
	require_once '../system-includes/template-cpanel-header.php';
	require_once '../system-includes/template-cpanel-aside.php';

?>

<main>
	
<h2 class="title-section">Редактирование товара</h2>

	<?php echo $system->get_notifications (); ?>

	<form action="?edit=<?php echo $product->id; ?>&cancel" method="POST" enctype="multipart/form-data">
		
		<div class="form-label">
			<label>
				<div class="input-title">Название товара:</div>
				<input id="js-name-input" value="<?php echo $product->getTitle (); ?>" type="text" name="name-item" maxlength="64">
			</label>
			<span><small><i>Название товара не должно превышать 64 символа</i></small></span>
		</div>

		<div class="form-label">
			<label>
				<div class="input-title">Стоимость:</div>
				<input id="js-cost-input" value="<?php echo $product->getCostNumber (); ?>" type="text" name="cost-item" maxlength="11">
			</label>
			<span><small><i>Цена товара не должна превышать 11 символов</i></small></span>
		</div>

		<div class="form-label">
			<label>
				<div class="input-title">Цена товара со скидкой:</div>
				<input id="js-cost-input" value="<?php echo $product->getSaleCostNumber (); ?>" type="text" name="sale-cost-item" maxlength="11">
			</label>
			<span><small><i>Цена товара не должна превышать 11 символов. Введите 0, чтобы отключить скидку</i></small></span>
		</div>

		<div class="form-label">
			<label>
				<div class="input-title">Дата окончания скидки:</div>
				<input id="js-cost-input" value="<?php echo $product->getEndDateOfSale (); ?>" type="text" name="sale-date-item" maxlength="11">
			</label>
			<span><small><i>Дата окончания скидки в формате ДД.ММ.ГГГГ или введите 0, чтобы отключить скидку</i></small></span>
		</div>

		<div class="form-label">
			<label>
				<div class="input-title">Время окончания скидки:</div>
				<input id="js-cost-input" value="<?php echo $product->getEndTimeOfSale (); ?>" type="text" name="sale-time-item" maxlength="11">
			</label>
			<span><small><i>Время, до которого действует скидка в последний день её действия, указанного в поле выше. В 24 часовом формате ЧЧ:ММ или ведите 0, чтобы отключить скидку</i></small></span>
		</div>

		<div class="form-label">
			<label>
				<div class="input-title">Краткое описание:</div>
				<textarea id="js-mini-desc-input" name="mini-description-item" maxlength="256" cols="30" rows="2"><?php echo $product->getMiniDescription (); ?></textarea>
			</label>
			<span><small><i>Краткое описание товара не должно превышать 256 символов</i></small></span>
		</div>

		<div class="form-label">
			<div class="input-title">Тип товара:</div>
				<label><input id="js-radio-normal" type="radio" name="type-item" value="normal" <?php echo ($product->getTypeOfItem () == 'normal') ? 'checked' : ''; ?>> одиночный товар</label> <br>
				<label><input id="js-radio-set" type="radio" name="type-item" value="set" <?php echo ($product->getTypeOfItem () == 'set') ? 'checked' : ''; ?>> набор</label>
		</div>

		<div class="form-label">
			<label>
				<div class="input-title">Полное описание:</div>
				<textarea name="description-item" class="forTinyMCE" cols="30" rows="10"><?php echo $product->getDescription (); ?></textarea>
			</label>
		</div>

		<div class="form-label">
			<label>
				<div class="input-title">Миниатюра:</div>
				<input id="js-mini-img-input" type="file" accept=".jpg, .jpeg, .png" name="mini_img">
			</label> <br>
			<span><small><i>Чтобы карточки товаров выглядели одинаково, все миниатюры должны быть одного размера.</i></small></span> <br>
			<span><small><i>Рекомендуемый размер: 350px X 350px (ширина X высота)</i></small></span> <br>

			<img src="<?php echo $product->getUrlImg (); ?>" alt="" width="350px">
		</div>

		<div class="form-label">
			<label>
				<div class="input-title">Изображения:</div>
				<input type="file" accept=".jpg, .jpeg, .png" name="files[]" multiple>
			</label>
		</div>

		<?php

		$images = $db->query ("SELECT `url` FROM `all_images` WHERE `id_item` = {$product->id}");

		while ($image = $images->fetch_assoc ()) {
			echo '<img class="mini-img-item" src="'.ABS_PATH.$image['url'].'">';
		}

		?>

		<input type="submit" value="Изменить">
		
	</form>

	<?php echo strtotime ('20.09.2020'); 
	echo ' '.date ('d.m.Y, G:i', strtotime ('20.09.2020') + 72000); ?>

	<br><small>* все поля обязательны для заполнения</small>


	<div class="preview normal" style="display: none;">
		<h3>Предварительный просмотр:</h3> <br>
		<div class='product hot'>
			<div class='info'>
				<img class="js-img-item" src='<?php echo $product->getUrlImg (); ?>' alt='Изображение товара'><hr>
				<h3 class='js-name-item name'><?php echo $product->getTitle (); ?></h3>
				<div class='js-cost-item cost'><?php echo $product->getCost (); ?></div>
				<hr>
			</div>
			<button class='standart-btn'>В корзину</button>
		</div>
	</div>

	<div class="preview set" style="display: none;">
		<h3>Предварительный просмотр:</h3> <br>
		<div class='product set '>
			<div class='info'>
				<h3 class='js-name-item name'><?php echo $product->getTitle (); ?></h3><hr>
				<img class="js-img-item" src='<?php echo $product->getUrlImg (); ?>' alt='Изображение товара Набор'>
				<p id="js-mini-desc-item"><?php echo $product->getMiniDescription (); ?></p>
				<hr>
				<div class='js-cost-item cost'><?php echo $product->getCost (); ?></div>
				<hr>
			</div>
			<button class='standart-btn'>В корзину</button>
		 </div>
	</div>

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

	<script>

		function fix_preview_block () {
			if ($("#js-name-input").outerWidth () + $(".preview.normal").outerWidth () > $("main").width ())
				$(".preview.normal").css ("position", "static")
			else
				$(".preview.normal").css ("position", "fixed")

			if ($("#js-name-input").outerWidth () + $(".preview.set").outerWidth () > $("main").width ())
				$(".preview.set").css ("position", "static")
			else
				$(".preview.set").css ("position", "fixed")

			$(".preview.normal button").css ("width", $(".preview.normal img").width ());
		}

		function nmb (str) {
			return str.replace (/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1\'');
		}


		var preview_type = $("input[name='type-item']:checked").val (),

			input_name      = $("#js-name-input"),
			input_cost      = $("#js-cost-input"),
			input_mini_desc = $("#js-mini-desc-input"),
			input_mini_img  = $("#js-mini-img-input"),

			item_name      = $(".js-name-item"),
			item_cost      = $(".js-cost-item"),
			item_mini_desc = $("#js-mini-desc-item"),
			item_mini_img  = $(".js-img-item");


		$(document).ready (function () {
			
			$(".preview." + preview_type).css ("display", "block");

			input_name[0].oninput = function () {
				item_name[0].innerHTML = input_name[0].value;
				item_name[1].innerHTML = input_name[0].value;
			}

			input_cost[0].oninput = function () {
				item_cost[0].innerHTML = nmb (input_cost[0].value) + " ₽";
				item_cost[1].innerHTML = nmb (input_cost[0].value) + " ₽";
			}

			input_mini_desc[0].oninput = function () {
				item_mini_desc[0].innerHTML = nmb (input_mini_desc[0].value);
			}

			input_mini_img[0].addEventListener ("change", function () {
				if (this.files[0]) {
					var fr = new FileReader ();
					fr.addEventListener ("load", function () {
						item_mini_img[0].src = fr.result;
						item_mini_img[1].src = fr.result;
					}, false);
					fr.readAsDataURL (this.files[0]);
				}
			});

			$('input[type=radio][name=type-item]').change (function () {
				if ($(this).val() != preview_type) {
					switch($(this).val()) {
						case "set": 
							var old_preview_type = 'normal'
							break;
						case "normal": 
							var old_preview_type = 'set'
							break;
					}
					preview_type = $(this).val();
					$(".preview." + old_preview_type).css ("display", "none");
					$(".preview." + preview_type).css ("display", "block");
				}
				$(".preview.normal button").css ("width", $(".preview.normal img").width ());
			});

			fix_preview_block ();

		});


		$(window).resize (function () {
			fix_preview_block ();
		});

	</script>

</main>

	<?php $system->destroy_notifications (); ?>

</main>