<?php

// подключение системных функций
	require_once ("system-core/main-functions.php");


// подключение шаблона шапки

	$system->set_active_page ('about-us');
	$system->set_title_of_page ('О нас');
	require_once ("system-includes/template-header.php");

	echo $system->decodeStr ($system->get_param ('page_about_us_content'));

// подключение шаблона футера
	require_once ("system-includes/template-footer.php");

?>