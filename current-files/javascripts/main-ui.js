/**
 * Created by Bernigend, https://bernigend.ru || https://vk.com/bernigend
 * Created on 03/2018 for the project of the store
 */

////   Слайдер  ////

var slideNow = 0;

// Размеры слайдов
function setSizeOfSlides () {
	$(".all-slides").css ("width", $(".slide").length * 100 + "%");
	$(".slide").css ("width", $(".slider").outerWidth ());
	return true;
}

// Следующий слайд
function sliderNext () {

	if ((slideNow + 1) < $(".slide").length) {
		$(".all-slides").animate ({
			"left": -$(".slider").outerWidth () * (slideNow + 1)
		}, 500);
		slideNow++;
	} else {
		slideNow = -1;
		sliderNext ();
	}

	return false;
}

// Предыдущий слайд
function sliderLast () {

	if ((slideNow - 1) >= 0) {
		$(".all-slides").animate ({
			"left": -$(".slider").outerWidth () * (slideNow - 1)
		}, 500);
		slideNow--;
	} else {
		slideNow = $(".slide").length;
		sliderLast ();
	}

	return false;

}


////   Ключевое оформление   ////


// Одинаковый размер названия товара у обычных товаров
function setSizesOfProducts (param) {

	var maxHeightName = new Array (),
		countRows     = 0;

	if ($("body").width () > 500) {

		if (param == "resize")
			$(".product .name").css ("height", "auto");

		// разбираем по полкам
		$(".products").each (function (index, element) {

			if ($(".product", element).hasClass ('set'))
				return;

			var countProductsInRow = $(".name", element).length - 1;
			
			// количество строк в полке
			if ($("body").width () > 640)
				countRows = Math.round ($(".name", element).length / 3);
			else
				countRows = Math.round ($(".name", element).length / 2);

			// разбираем по строчкам
			for (var rowNow = 0; rowNow < countRows; rowNow++) {

				maxHeightName [rowNow] = 0;

				if ($("body").width () > 624) {

					if (countProductsInRow >= (0 + 3 * rowNow) && $(".name", element)[0 + 3 * rowNow].offsetHeight > maxHeightName [rowNow])
						maxHeightName [rowNow] = $(".name", element)[0 + 3 * rowNow].offsetHeight;

					if (countProductsInRow >= (1 + 3 * rowNow) && $(".name", element)[1 + 3 * rowNow].offsetHeight > maxHeightName [rowNow])
						maxHeightName [rowNow] = $(".name", element)[1 + 3 * rowNow].offsetHeight;

					if (countProductsInRow >= (2 + 3 * rowNow) && $(".name", element)[2 + 3 * rowNow].offsetHeight > maxHeightName [rowNow])
						maxHeightName [rowNow] = $(".name", element)[2 + 3 * rowNow].offsetHeight;

				} else {

					if (countProductsInRow >= (0 + 2 * rowNow) && $(".name", element)[0 + 2 * rowNow].offsetHeight > maxHeightName [rowNow])
						maxHeightName [rowNow] = $(".name", element)[0 + 2 * rowNow].offsetHeight;

					if (countProductsInRow >= (1 + 2 * rowNow) && $(".name", element)[1 + 2 * rowNow].offsetHeight > maxHeightName [rowNow])
						maxHeightName [rowNow] = $(".name", element)[1 + 2 * rowNow].offsetHeight;

				}

			}


			for (var rowNow = 0; rowNow < countRows; rowNow++) {

				if ($("body").width () > 624) {

					if (countProductsInRow >= (0 + 3 * rowNow))
						$(".name", element)[0 + 3 * rowNow].style.height = maxHeightName [rowNow] + "px";

					if (countProductsInRow >= (1 + 3 * rowNow))
						$(".name", element)[1 + 3 * rowNow].style.height = maxHeightName [rowNow] + "px";

					if (countProductsInRow >= (2 + 3 * rowNow))
						$(".name", element)[2 + 3 * rowNow].style.height = maxHeightName [rowNow] + "px";

				} else if ($("body").width () <= 640 && !$(".product.set", element).length) {

					if (countProductsInRow >= (0 + 2 * rowNow))
						$(".name", element)[0 + 2 * rowNow].style.height = maxHeightName [rowNow] + "px";

					if (countProductsInRow >= (1 + 2 * rowNow))
						$(".name", element)[1 + 2 * rowNow].style.height = maxHeightName [rowNow] + "px";

				}
			}

		});

	}

	return true;

}


// Одинаковые названия и описания у товаров-наборов
function setSizesOfSets (param) {

	if ($("body").width () < 624) {
		$(".product.set .name").css ("height", "auto");
		$(".product.set p").css ("height", "auto");
		return false;
	}

	var maxHeightName = new Array (),
		maxHeightP    = new Array ();

	$(".products").each (function (index, element) {

		if (!$(".product", element).hasClass ('set'))
			return;

		if (param == "resize") {
			$(".name", element).css ("height", "auto");
			$("p", element).css ("height", "auto");
		}

		var countProductsInRow = $(".name", element).length - 1,
			countRows          = Math.round ($(".product.set", element).length / 2);

		for (var rowNow = 0; rowNow < countRows; rowNow++) {
			maxHeightName [rowNow] = 0;
			maxHeightP [rowNow]    = 0;

			if (countProductsInRow >= (0 + 2 * rowNow) && $(".name", element)[0 + 2 * rowNow].offsetHeight > maxHeightName [rowNow])
				maxHeightName [rowNow] = $(".name", element)[0 + 2 * rowNow].offsetHeight;

			if (countProductsInRow >= (1 + 2 * rowNow) && $(".name", element)[1 + 2 * rowNow].offsetHeight > maxHeightName [rowNow])
				maxHeightName [rowNow] = $(".name", element)[1 + 2 * rowNow].offsetHeight;

			if (countProductsInRow >= (0 + 2 * rowNow) && $("p", element)[0 + 2 * rowNow].offsetHeight > maxHeightP [rowNow])
				maxHeightP [rowNow] = $("p", element)[0 + 2 * rowNow].offsetHeight;

			if (countProductsInRow >= (1 + 2 * rowNow) && $("p", element)[1 + 2 * rowNow].offsetHeight > maxHeightP [rowNow])
				maxHeightP [rowNow] = $("p", element)[1 + 2 * rowNow].offsetHeight;
		}

		for (var rowNow = 0; rowNow < countRows; rowNow++) {

			if (countProductsInRow >= (0 + 2 * rowNow))
				$(".name", element)[0 + 2 * rowNow].style.height = maxHeightName [rowNow] + "px";

			if (countProductsInRow >= (1 + 2 * rowNow))
				$(".name", element)[1 + 2 * rowNow].style.height = maxHeightName [rowNow] + "px";

			if (countProductsInRow >= (0 + 2 * rowNow))
				$("p", element)[0 + 2 * rowNow].style.height = maxHeightP [rowNow] + "px";

			if (countProductsInRow >= (1 + 2 * rowNow))
				$("p", element)[1 + 2 * rowNow].style.height = maxHeightP [rowNow] + "px";
		}

	});

}


function scrollUp () {
	$("html, body").animate ({ scrollTop: 0 }, 500);
	return false;
}


function openCart () {
	$("#js-cart-open-place, #js-cart-open-place-black").stop().fadeIn ();
	return true;
}

function closeCart () {
	$("#js-cart-open-place, #js-cart-open-place-black").stop().fadeOut ();
	$("#js-cart-items, #js-cart-items-btn").css ("display", "block");
	$("#js-user-info, #js-user-info-btn").css ("display", "none");
	return true;
}

function executeCart () {
	$("#js-cart-items, #js-cart-items-btn").css ("display", "none");
	$("#js-user-info, #js-user-info-btn").css ("display", "block");
	return true;
}

function addToCart (elem) {
	var item = $(elem).parent();
	console.log (item.attr ("data-id"));
	console.log ($(".name", item).html ());
	console.log (item.attr ("data-cost"));
	return true;
}


var maxHeightName = 0,
	maxHeightP    = 0,
	arrowFaded = false,
	cartFaded = false;


$(document).ready (function () {

	// Приводим кнопки "В корзину" к одному размеру
	$(".product").each (function (index, element) {
		$("button", element).css ("width", $("img", element).width ());
	});

	// Одинаковые размеры у товаров
	setSizesOfProducts ();

	// Одинаковые размеры у слайдов
	setSizeOfSlides ();

	// Одинаковые размеры у наборов
	setSizesOfSets ();

});

$(window).resize (function () {

	// Приводим кнопки "В корзину" к одному размеру
	$(".product").each (function (index, element) {
		$("button", element).css ("width", $("img", element).width ());
	});

	// Одинаковые размеры у товаров
	setSizesOfProducts ("resize");

	// Одинаковые размеры у слайдов
	setSizeOfSlides ();

	// Одинаковые размеры у наборов
	setSizesOfSets ("resize");

});

$(window).scroll (function (event) {

	var scrollTop = $("html").scrollTop ();

	if (scrollTop > 50 && !arrowFaded) {
		arrowFaded = true;
		$(".arrow-up").stop().fadeIn ();
	}

	if (scrollTop <= 50 && arrowFaded) {
		arrowFaded = false;
		$(".arrow-up").stop().fadeOut ();
	}

	if (scrollTop > $("#js-navigation").offset().top && !cartFaded) {
		$("#js-cart").fadeIn ();
		cartFaded = true;
	} else if (scrollTop <= $("#js-navigation").offset().top && cartFaded == true) {
		$("#js-cart").fadeOut ();
		cartFaded = false;
	}

});