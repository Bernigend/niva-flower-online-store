## Описание
---
Репозиторий исходников системы интернет-магазина (урезанной версии)

Создан как проект на курсах по программированию (обр. учреждение "Нива")

Авторы: Bernigend - https://vk.com/id222734159 || https://bernigend.ru

Дата создания: 21.03.2018 (дд.мм.гггг)

## Структура Файлов
---
  - backup-databases: резервные копии баз данных
  - current-files: исходники уже переписанного сайта на PHP
  - sample-design: исходники изначального дизайна сайта без доработок на PHP
  
## Структура папки "current-files"
---
  - control-panel/ (файлы панели управления)
  - system-core/ (ядро системы)
    - ../class.product.php (класс отдельного товара)
	- ../class.system.php (класс системных функций)
	- ../class.user.php (класс отдельного пользователя)
	- ../config-system.php (основные конфигурации системы)
	- ../config-system.php.sample (основные конфигурации системы; пример)
	- ../main-functions.php (основные функции системы)
  - system-includes/ (дополнительные файлы системы)
    - ../template-cpanel-aside.php (навигация панели управления)
	- ../template-cpanel-authorization.php (авторизация панели управления)
	- ../template-cpanel-header.php (шапка панели управления)
	- ../template-cpanel-index.php (главная страница панели)
	- ../template-footer.php (низ основных страниц сайта)
	- ../template-header.php (шапка основных страниц сайта)